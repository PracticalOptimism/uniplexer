
const path = require('path')

module.exports = {
  target: 'node',
  mode: 'development',
  devServer: {
    contentBase: path.join(__dirname, 'compiled'),
    port: 8080,
    host: `localhost`,
  },
  entry: {
    httpServer: './precompiled/http-server.ts'
  },
  output: {
    filename: 'http-server.js',
    libraryTarget: 'umd',
    path: path.resolve(__dirname, 'compiled')
  },
  module: {
    rules: [{
      test: /\.tsx?$/,
      use: 'ts-loader',
      exclude: /node_modules/
    }]
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
    alias: {
      'precompiled': path.resolve(__dirname, './precompiled'),
      '@': path.resolve(__dirname, './precompiled')
    }
  },
  devtool: false
}

