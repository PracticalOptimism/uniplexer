
function updateObjectValueFunction (object: any, keyList: string[], value: any, originalObject?: any): any {
  if (!object) {
    object = {}
  }
  if (!originalObject) {
    originalObject = object
  }
  if (keyList.length === 0) {
    object = originalObject
    return originalObject
  }
  if (keyList.length === 1) {
    object[keyList[0]] = value
    object = originalObject
    return originalObject
  }
  if (!object[keyList[0]]) {
    object[keyList[0]] = {}
  }
  return updateObjectValueFunction(object[keyList[0]], keyList.slice(1), value, originalObject)
}

const updateObjectValue = {
  function: updateObjectValueFunction
}

export {
  updateObjectValue
}
