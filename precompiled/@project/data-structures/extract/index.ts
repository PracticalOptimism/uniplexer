
class ProjectExtract {
  usecase?: ProjectUsecaseExtract = new ProjectUsecaseExtract()

  constructor (defaultProjectExtract?: ProjectExtract) {
    Object.assign(this, defaultProjectExtract)
  }
}

class ProjectUsecaseExtract {
  providerMultiplexer?: ProjectUsecaseProviderMultiplexerExtract = new ProjectUsecaseProviderMultiplexerExtract()

  constructor (defaultProjectUsecaseExtract?: ProjectUsecaseExtract) {
    Object.assign(this, defaultProjectUsecaseExtract)
  }
}

class ProjectUsecaseProviderMultiplexerExtract {
  providerTable?: { [providerType: string]: { [providerNameVersionId: string]: boolean } } = {}
  providerCredentialTable?: { [providerCredentialId: string]: boolean } = {}

  constructor (defaultProjectUsecaseProviderMultiplexerExtract?: ProjectUsecaseProviderMultiplexerExtract) {
    Object.assign(this, defaultProjectUsecaseProviderMultiplexerExtract)
  }
}

export {
  ProjectExtract,
  ProjectUsecaseExtract,
  ProjectUsecaseProviderMultiplexerExtract
}

