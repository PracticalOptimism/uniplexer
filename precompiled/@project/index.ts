

import * as constantsProject from './constants/project'

const constants = {
  project: { ...constantsProject }
}

export {
  constants
}
