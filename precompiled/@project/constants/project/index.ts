
const PROJECT_DESCRIPTION_OBJECT = {
  projectId: 'uniplexer',
  projectName: 'Uniplexer',
  projectOrganizationId: '',
  projectOrganizationName: '',
  projectNameIdentifierGroup: {},
  projectWebsiteIdentifierGroup: {},

  projectJavaScriptLibraryId: 'uniplexer',
  projectTextDescriptionGroup: {},
  projectEnvironmentOperationOptions: {}

  // projectDescriptionTopic:
}

/*
class ProjectDescriptionTopic {
  projectDescriptionTopicId: string = ''
  projectDescriptionTopicName: string = ''
  projectDescriptionTopicNameDescription: string = ''
  projectDescriptionTopicAddressIdentifierLocationSpecifier: string = ''
  programOperationEnvironment: string = ''
  operatingSystemSpecifyingEnvironment: string = ''
  operatingSystemSpecifyingOrganization: string = ''
  operatingSystemSpecifyingOrganizationPublicOfficialWebsite: string = ''
}

*/

export {
  PROJECT_DESCRIPTION_OBJECT
}
