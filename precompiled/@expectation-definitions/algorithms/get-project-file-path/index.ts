

function getProjectFilePathFunction (filePathName: string) {
  const indexOfPrecompiledFileName = filePathName.indexOf('/precompiled')
  const indexOfExpectationDefinitionFileName = filePathName.indexOf('/index.expectation-definitions.ts')
  return filePathName.slice(indexOfPrecompiledFileName, indexOfExpectationDefinitionFileName)
}

const getProjectFilePath = {
  function: getProjectFilePathFunction
}

export {
  getProjectFilePath
}
