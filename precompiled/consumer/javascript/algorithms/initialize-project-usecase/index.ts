
import { javascriptLibrary } from '../../variables/javascript'

async function initializeProjectUsecaseFunction () {

  // Initialize providerMultiplexer usecase
  javascriptLibrary.usecase.providerMultiplexer.algorithms.providerMultiplexer.initializeProviderMultiplexerTable.function()
}

const initializeProjectUsecase = {
  function: initializeProjectUsecaseFunction
}

export {
  initializeProjectUsecase
}
