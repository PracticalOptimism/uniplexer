import { javascriptLibrary } from '../../variables/javascript'

async function initializeProjectProviderFunction () {
  // Initialize current device operation settings
  javascriptLibrary.provider._device.algorithms.initializeCurrentDeviceOperationSettings.function()
}

const initializeProjectProvider = {
  function: initializeProjectProviderFunction
}

export {
  initializeProjectProvider
}
