

import { javascriptLibrary } from '../../variables/javascript'

import { initializeProjectUsecase } from '../initialize-project-usecase'
import { initializeProjectProvider } from '../initialize-project-provider'
import { Provider } from '../../../../usecase/provider-multiplexer/data-structures/provider'
import { PROVIDER_TYPE_DATABASE, PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION } from '../../../../usecase/provider-multiplexer/constants/provider-type'

async function initializeJavascriptLibraryFunction () {
  await initializeProjectProvider.function()
  await initializeProjectUsecase.function()

  // Initialize operating environment and extract constants
  const presentOperatingEnvironmentId = javascriptLibrary.provider._device.variables.device.nonObjectReference.currentDeviceOperationSettings.operatingEnvironmentId

  // Add default provider and provider credentials for easy
  // javascript library usecase and convenience

  let providerServiceTreeList: any[] = Object.keys(javascriptLibrary.provider.service).map((serviceId: string) => (javascriptLibrary.provider.service as any)[serviceId])
  let addedProviderList: Provider[] = []

  for (let i = 0; i < providerServiceTreeList.length; i++) {
    const providerServiceTreeItem: any = providerServiceTreeList[i]

    if (!providerServiceTreeItem) { continue }

    if (!providerServiceTreeItem.constants) {
      providerServiceTreeList = providerServiceTreeList.concat(Object.keys(providerServiceTreeItem).map((serviceId: string) => (providerServiceTreeItem)[serviceId]))
      continue
    }

    const provider = providerServiceTreeItem.constants.SERVICE_PROVIDER as Provider
    if (provider.isMockProvider) { continue }
    if (!provider.operatingSystemAndOpenAccessHardwareSpecificationsForPresentComputerOperatingSystem) { continue }
    if (!provider.operatingSystemAndOpenAccessHardwareSpecificationsForPresentComputerOperatingSystem[presentOperatingEnvironmentId]) { continue }

    // Add provider
    javascriptLibrary.usecase.providerMultiplexer.algorithms.provider.addProvider.function(provider, {}, true)
    addedProviderList.push(provider)
  }

  // Initialize default cryptographic hash function provider
  let cryptographicHashFunctionProviderList = javascriptLibrary.usecase.providerMultiplexer.algorithms.provider.getProviderList.function(PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION, undefined, presentOperatingEnvironmentId)
  cryptographicHashFunctionProviderList.sort((provider1, provider2) => {
    if (provider1.providerNameVersionId < provider2.providerNameVersionId) {
      return -1
    } else if (provider1.providerNameVersionId > provider2.providerNameVersionId) {
      return 1
    } else {
      return 0
    }
  })
  const defaultCryptographicHashFunctionProvider = cryptographicHashFunctionProviderList[0]
  if (defaultCryptographicHashFunctionProvider) {
    javascriptLibrary.usecase.providerMultiplexer.algorithms.provider.initializeDefaultProvider.function(defaultCryptographicHashFunctionProvider.providerType, defaultCryptographicHashFunctionProvider.providerNameVersionId)
  }

  // Initialize default database provider
  let databaseProviderList = javascriptLibrary.usecase.providerMultiplexer.algorithms.provider.getProviderList.function(PROVIDER_TYPE_DATABASE, undefined, presentOperatingEnvironmentId)
  databaseProviderList.sort((provider1, provider2) => {
    if (provider1.providerNameVersionId < provider2.providerNameVersionId) {
      return -1
    } else if (provider1.providerNameVersionId > provider2.providerNameVersionId) {
      return 1
    } else {
      return 0
    }
  })
  const defaultDatabaseProvider = databaseProviderList[0]
  if (defaultDatabaseProvider) {
    javascriptLibrary.usecase.providerMultiplexer.algorithms.provider.initializeDefaultProvider.function(defaultDatabaseProvider.providerType, defaultDatabaseProvider.providerNameVersionId)
  }

  // Add provider credential for services that have a default configuration object
  for (let i = 0; i < addedProviderList.length; i++) {
    const provider = addedProviderList[i]
    await javascriptLibrary.usecase.providerMultiplexer.algorithms.providerCredential.addProviderCredential.function({
      providerNameVersionId: provider.providerNameVersionId,
      configurationSettingsTable: provider.providerDefaultConfigurationSettingsObject || {}
    })
  }
}

const initializeJavascriptLibrary = {
  function: initializeJavascriptLibraryFunction
}

export {
  initializeJavascriptLibrary
}

