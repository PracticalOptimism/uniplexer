
import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../..//../../@expectation-definitions/algorithms/get-project-file-path'
import { initializeJavascriptLibrary } from './index'

import { javascriptLibrary } from '../../variables/javascript'


describe(getProjectFilePath.function(__filename), () => {
  after(async () => {
    // delete default provider list
    const providerList = javascriptLibrary.usecase.providerMultiplexer.algorithms.provider.getProviderList.function()
    for (let providerListItemIndex = 0; providerListItemIndex < providerList.length; providerListItemIndex++) {
      const provider = providerList[providerListItemIndex]
      await javascriptLibrary.usecase.providerMultiplexer.algorithms.provider.deleteProvider.function(provider.providerNameVersionId)
    }
  })
  it('should initialize the provider table with default provider services', async () => {
    const providerListBeforeInitialization = javascriptLibrary.usecase.providerMultiplexer.algorithms.provider.getProviderList.function()

    expect(providerListBeforeInitialization.length).to.be.equal(0)

    await initializeJavascriptLibrary.function()

    const providerListAfterInitialization = javascriptLibrary.usecase.providerMultiplexer.algorithms.provider.getProviderList.function()

    expect(providerListAfterInitialization.length).to.be.greaterThan(0)
  })
})

