

import { javascriptLibrary, javascriptGlobalVariable } from '../../variables/javascript'

function addJavascriptLibraryToGlobalEnvironmentFunction () {
  if (javascriptGlobalVariable) {
    (javascriptGlobalVariable as any)[javascriptLibrary._project.constants.project.PROJECT_DESCRIPTION_OBJECT.projectJavaScriptLibraryId] = javascriptLibrary
  }
}

const addJavascriptLibraryToGlobalEnvironment = {
  function: addJavascriptLibraryToGlobalEnvironmentFunction
}

export {
  addJavascriptLibraryToGlobalEnvironment
}
