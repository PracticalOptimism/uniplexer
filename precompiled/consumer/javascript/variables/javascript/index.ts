
import * as _projectModule from '../../../../@project'

// import * as consumerHttpServer from '../../http-server'
import * as usecaseProviderMultiplexer from '../../../../usecase/provider-multiplexer'

import * as providerDevice from '../../../../provider/@device'
import * as providerService from '../../../../provider/service'

const javascriptLibrary = {
  _project: { ..._projectModule },

  consumer: {
    javascript: {}
    // httpServer: { ...consumerHttpServer }
  },

  usecase: {
    providerMultiplexer: { ...usecaseProviderMultiplexer }
  },

  provider: {
    _device: { ...providerDevice },
    service: { ...providerService }
  }
}

const javascriptGlobalVariable = this || window || global || globalThis

export {
  javascriptLibrary,
  javascriptGlobalVariable
}
