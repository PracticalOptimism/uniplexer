
import * as indexAlgorithms from './algorithms'

import * as javascriptVariables from './variables/javascript'

const algorithms = { ...indexAlgorithms }

const variables = {
  javascript: { ...javascriptVariables }
}

// This is used to prevent importing algorithms in the
// variable files which may otherwise result in circular
// imports. Circular imports aren't supported by webpack
// at the moment this comment was made. Webpack is a
// tool that helps make this project work, at this time.
javascriptVariables.javascriptLibrary.consumer.javascript = {
  algorithms,
  variables
}

export {
  algorithms,
  variables
}

