
import * as javascriptConsumer from './consumer/javascript'

javascriptConsumer.algorithms.addJavascriptLibraryToGlobalEnvironment.function()
javascriptConsumer.algorithms.initializeJavascriptLibrary.function().catch((err) => {
  console.log(`Error initializing JavaScript Library: ${err}`)
})

const javascriptLibrary = javascriptConsumer.variables.javascript.javascriptLibrary

export {
  javascriptLibrary
}

