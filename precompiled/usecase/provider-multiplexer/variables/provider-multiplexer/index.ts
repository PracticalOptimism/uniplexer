

import * as providerMultiplexerDataStructures from '../../data-structures/provider-multiplexer'
import * as providerTypeConstants from '../../constants/provider-type'


const providerMultiplexerTable: {
  [providerTypeConstants.PROVIDER_TYPE_ASYMMETRIC_CRYPTOGRAPHY]?: providerMultiplexerDataStructures.ProviderMultiplexer<providerMultiplexerDataStructures.asymmetricCryptography.AsymmetricCryptographyProviderFunctionTable>
  [providerTypeConstants.PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION]?: providerMultiplexerDataStructures.ProviderMultiplexer<providerMultiplexerDataStructures.cryptographicHashFunction.CryptographicHashFunctionProviderFunctionTable>
  [providerTypeConstants.PROVIDER_TYPE_DATABASE]?: providerMultiplexerDataStructures.ProviderMultiplexer<providerMultiplexerDataStructures.database.DatabaseProviderFunctionTable>
  [providerType: string]: any
} = {}

// Create a second map that allows for easy access to the function table without
// referencing providerMultiplexer.providerMultiplexerFunctionTable
const providerMultiplexerFunctionTableMap: {
  [providerTypeConstants.PROVIDER_TYPE_ASYMMETRIC_CRYPTOGRAPHY]?: providerMultiplexerDataStructures.asymmetricCryptography.AsymmetricCryptographyProviderFunctionTable
  [providerTypeConstants.PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION]?: providerMultiplexerDataStructures.cryptographicHashFunction.CryptographicHashFunctionProviderFunctionTable
  [providerTypeConstants.PROVIDER_TYPE_DATABASE]?: providerMultiplexerDataStructures.database.DatabaseProviderFunctionTable
  [providerType: string]: any
} = {}

export {
  providerMultiplexerTable,
  providerMultiplexerFunctionTableMap
}

