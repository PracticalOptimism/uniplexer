import { Provider } from '../../data-structures/provider'
import { ProviderCredential } from '../../data-structures/provider-credential'

const providerTable: { [providerNameVersionId: string]: Provider } = {}

const providerCredentialTable: { [providerCredentialId: string]: ProviderCredential } = {}

const defaultProviderTable: { [providerType: string]: string } = {}

export {
  providerTable,
  providerCredentialTable,
  defaultProviderTable
}

