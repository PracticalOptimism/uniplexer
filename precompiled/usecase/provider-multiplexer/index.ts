

import * as providerAlgorithms from './algorithms/provider'
import * as providerCredentialAlgorithms from './algorithms/provider-credential'
import * as providerMultiplexerAlgorithms from './algorithms/provider-multiplexer'

import * as providerConstants from './constants/provider'
import * as providerCredentialConstants from './constants/provider-credential'
import * as providerTypeConstants from './constants/provider-type'
import * as providerMultiplexerConstants from './constants/provider-multiplexer'

import * as providerDataStructures from './data-structures/provider'
import * as providerCredentialDataStructures from './data-structures/provider-credential'
import * as providerMultiplexerDataStructures from './data-structures/provider-multiplexer'

import * as providerVariables from './variables/provider'
import * as providerMultiplexerVariables from './variables/provider-multiplexer'

const algorithms = {
  provider: { ...providerAlgorithms },
  providerCredential: { ...providerCredentialAlgorithms },
  providerMultiplexer: { ...providerMultiplexerAlgorithms }
}

const constants = {
  provider: { ...providerConstants },
  providerCredential: { ...providerCredentialConstants },
  providerType: { ...providerTypeConstants },
  providerMultiplexer: { ...providerMultiplexerConstants }
}

const dataStructures = {
  provider: { ...providerDataStructures },
  providerCredential: { ...providerCredentialDataStructures },
  providerMultiplexer: { ...providerMultiplexerDataStructures }
}

const variables = {
  provider: { ...providerVariables },
  providerMultiplexer: { ...providerMultiplexerVariables }
}

export {
  algorithms,
  constants,
  dataStructures,
  variables
}

