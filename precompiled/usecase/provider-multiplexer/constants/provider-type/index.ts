

const PROVIDER_TYPE_ASYMMETRIC_CRYPTOGRAPHY = 'asymmetricCryptography'
const PROVIDER_TYPE_COMPUTER_RESOURCE_SERVICE = 'computerResourceService'
const PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION = 'cryptographicHashFunction'
const PROVIDER_TYPE_CURRENCY_SERVICE = 'currencyService'
const PROVIDER_TYPE_DATABASE = 'database'
const PROVIDER_TYPE_DISTRIBUTED_LEDGER_TECHNOLOGY = 'distributedLedgerTechnology'
const PROVIDER_TYPE_MESSAGE_STREAMING_SERVICE = 'messageStreamingService'
const PROVIDER_TYPE_NETWORK_PROTOCOL = 'networkProtocol'
const PROVIDER_TYPE_TEXT_SEARCH = 'textSearch'

const PROVIDER_TYPE_LIST = [
  PROVIDER_TYPE_ASYMMETRIC_CRYPTOGRAPHY,
  PROVIDER_TYPE_COMPUTER_RESOURCE_SERVICE,
  PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION,
  PROVIDER_TYPE_CURRENCY_SERVICE,
  PROVIDER_TYPE_DATABASE,
  PROVIDER_TYPE_DISTRIBUTED_LEDGER_TECHNOLOGY,
  PROVIDER_TYPE_MESSAGE_STREAMING_SERVICE,
  PROVIDER_TYPE_NETWORK_PROTOCOL,
  PROVIDER_TYPE_TEXT_SEARCH
]

export {
  PROVIDER_TYPE_ASYMMETRIC_CRYPTOGRAPHY,
  PROVIDER_TYPE_COMPUTER_RESOURCE_SERVICE,
  PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION,
  PROVIDER_TYPE_CURRENCY_SERVICE,
  PROVIDER_TYPE_DATABASE,
  PROVIDER_TYPE_DISTRIBUTED_LEDGER_TECHNOLOGY,
  PROVIDER_TYPE_MESSAGE_STREAMING_SERVICE,
  PROVIDER_TYPE_NETWORK_PROTOCOL,
  PROVIDER_TYPE_TEXT_SEARCH,
  PROVIDER_TYPE_LIST
}
