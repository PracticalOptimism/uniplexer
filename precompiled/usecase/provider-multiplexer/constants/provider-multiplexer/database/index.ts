

const FUNCTION_NAME_CREATE_ITEM = 'createItem'
const FUNCTION_NAME_CREATE_ITEM_ID = 'createItemId'
const FUNCTION_NAME_CREATE_ITEM_STORE = 'createItemStore'
const FUNCTION_NAME_GET_ITEM = 'getItem'
const FUNCTION_NAME_GET_ITEM_LIST = 'getItemList'
const FUNCTION_NAME_UPDATE_ITEM = 'updateItem'
const FUNCTION_NAME_DELETE_ITEM = 'deleteItem'
const FUNCTION_NAME_DOES_EXIST_ITEM = 'doesExistItem'
const FUNCTION_NAME_ADD_ITEM_EVENT_LISTENER = 'addItemEventListener'
const FUNCTION_NAME_REMOVE_ITEM_EVENT_LISTENER = 'removeItemEventListener'

const FUNCTION_NAME_LIST = [
  FUNCTION_NAME_CREATE_ITEM,
  FUNCTION_NAME_CREATE_ITEM_ID,
  FUNCTION_NAME_CREATE_ITEM_STORE,
  FUNCTION_NAME_GET_ITEM,
  FUNCTION_NAME_GET_ITEM_LIST,
  FUNCTION_NAME_UPDATE_ITEM,
  FUNCTION_NAME_DELETE_ITEM,
  FUNCTION_NAME_DOES_EXIST_ITEM,
  FUNCTION_NAME_ADD_ITEM_EVENT_LISTENER,
  FUNCTION_NAME_REMOVE_ITEM_EVENT_LISTENER
]

export {
  FUNCTION_NAME_CREATE_ITEM,
  FUNCTION_NAME_CREATE_ITEM_ID,
  FUNCTION_NAME_CREATE_ITEM_STORE,
  FUNCTION_NAME_GET_ITEM,
  FUNCTION_NAME_GET_ITEM_LIST,
  FUNCTION_NAME_UPDATE_ITEM,
  FUNCTION_NAME_DELETE_ITEM,
  FUNCTION_NAME_DOES_EXIST_ITEM,
  FUNCTION_NAME_ADD_ITEM_EVENT_LISTENER,
  FUNCTION_NAME_REMOVE_ITEM_EVENT_LISTENER,
  FUNCTION_NAME_LIST
}

