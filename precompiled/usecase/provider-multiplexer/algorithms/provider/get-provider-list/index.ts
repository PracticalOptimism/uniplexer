
import { ProjectExtract } from '../../../../../@project/data-structures/extract'
import { Provider } from '../../../data-structures/provider'
import { providerTable } from '../../../variables/provider'

import { providerMultiplexerTable } from '../../../variables/provider-multiplexer'

function getProviderListFunction (providerType?: string, hasOfflineSupport?: boolean, operatingEnvironmentId?: string, _extract?: ProjectExtract): Provider[] {

  if (!providerType || !providerMultiplexerTable[providerType]) {
    return Object.keys(providerTable).map((providerNameVersionId: string) => providerTable[providerNameVersionId])
  }

  let providerListByType = providerMultiplexerTable[providerType].providerNameVersionIdList.map((providerNameVersionId: string) => providerTable[providerNameVersionId])

  // return all of the providers of the given type
  if (hasOfflineSupport !== undefined) {
    providerListByType = providerListByType.filter((provider: Provider) => hasOfflineSupport === provider.hasOfflineSupport)
  }

  if (operatingEnvironmentId !== undefined) {
    providerListByType = providerListByType.filter((provider: Provider) => {
      if (!provider.operatingSystemAndOpenAccessHardwareSpecificationsForPresentComputerOperatingSystem) { return false }
      return provider.operatingSystemAndOpenAccessHardwareSpecificationsForPresentComputerOperatingSystem[operatingEnvironmentId] ? true : false
    })
  }

  // select the providers with the provided hasOfflineSupport options
  return providerListByType
}

const getProviderList = {
  function: getProviderListFunction
}

export {
  getProviderList
}
