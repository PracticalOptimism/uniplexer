
import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'

import { getProviderList } from './index'

import { initializeProviderTable, deleteProviderTable } from '../add-provider/index.expectation-definitions'

import { PROVIDER_TYPE_DATABASE } from '../../../constants/provider-type'

describe(getProjectFilePath.function(__filename), () => {
  before(() => {
    initializeProviderTable()
  })
  after(async () => {
    await deleteProviderTable()
  })
  it('should get list of providers from providerTable', async () => {

    // Get provider
    const databaseProviderList = getProviderList.function(PROVIDER_TYPE_DATABASE)

    // Expect there to be more than 0 database providers available
    expect(databaseProviderList.length).to.be.greaterThan(0)
  })
})
