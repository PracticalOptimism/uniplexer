
import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'

import { addProvider } from './index'
import { deleteProvider } from '../delete-provider'

import { Provider } from '../../../data-structures/provider'
import { providerTable, defaultProviderTable } from '../../../variables/provider'

import { asymmetricCryptography, database, cryptographicHashFunction } from '../../../../../provider/service/@mock'
import { PROVIDER_TYPE_ASYMMETRIC_CRYPTOGRAPHY, PROVIDER_TYPE_DATABASE, PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION } from '../../../constants/provider-type'
import { initializeDefaultProvider } from '../initialize-default-provider'

const NUMBER_OF_PROVIDERS_PER_TYPE = 3

const PROVIDER_TABLE: { [providerType: string]: Provider } = {
  [PROVIDER_TYPE_ASYMMETRIC_CRYPTOGRAPHY]: asymmetricCryptography.constants.SERVICE_PROVIDER,
  [PROVIDER_TYPE_DATABASE]: database.constants.SERVICE_PROVIDER,
  [PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION]: cryptographicHashFunction.constants.SERVICE_PROVIDER
}

function initializeProviderTable () {
  for (const providerType in PROVIDER_TABLE) {
    if (!PROVIDER_TABLE.hasOwnProperty(providerType)) { continue }

    // Add providers for this type
    for (let i = 0; i < NUMBER_OF_PROVIDERS_PER_TYPE; i++) {
      const mockProvider = new Provider(PROVIDER_TABLE[providerType])
      mockProvider.providerNameVersionId = `mock-${providerType}@${i}.0.0`
      addProvider.function(mockProvider)
    }

    // Initialize default provider for this type
    initializeDefaultProvider.function(providerType, `mock-${providerType}@${0}.0.0`)
  }
}

async function deleteProviderTable (): Promise<boolean[]> {
  const deleteResultList = []
  for (const providerType in PROVIDER_TABLE) {
    if (!PROVIDER_TABLE.hasOwnProperty(providerType)) { continue }

    // Remove default provider type
    delete defaultProviderTable[providerType]

    // Remove default providers
    for (let i = 0; i < NUMBER_OF_PROVIDERS_PER_TYPE; i++) {
      const deleteResult = await deleteProvider.function(`mock-${providerType}@${i}.0.0`)
      deleteResultList.push(deleteResult)
    }
  }
  return deleteResultList
}

describe(getProjectFilePath.function(__filename), () => {
  it('should add provider to providerTable', async () => {
    // Expect there to be exactly 0 providers in the providerTable
    expect(Object.keys(providerTable).length).eq(0)

    // Add providers to the providerTable
    initializeProviderTable()

    // Expect there to be providers in the providerTable
    const numberOfExpectedProviders = NUMBER_OF_PROVIDERS_PER_TYPE * Object.keys(PROVIDER_TABLE).length
    expect(Object.keys(providerTable).length).eq(numberOfExpectedProviders)

    // Remove the providers from the providerTable
    await deleteProviderTable()
  })
})

export {
  NUMBER_OF_PROVIDERS_PER_TYPE,
  PROVIDER_TABLE,
  initializeProviderTable,
  deleteProviderTable
}
