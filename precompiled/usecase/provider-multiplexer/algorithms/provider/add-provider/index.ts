
import { ProjectExtract } from '../../../../../@project/data-structures/extract'
import { Provider } from '../../../data-structures/provider'
import { providerTable } from '../../../variables/provider'
import { ProviderMultiplexer } from '../../../data-structures/provider-multiplexer'

import { providerMultiplexerTable } from '../../../variables/provider-multiplexer'

function addProviderFunction (providerObject: Provider, providerConfigurationObject?: any, shouldCallInitializeProvider?: boolean, extract?: ProjectExtract): Provider {
  const provider = new Provider(providerObject)

  if (!provider.providerNameVersionId) {
    throw Error(`Cannot add provider to provider table without a providerNameVersionId on providerOption: ${providerObject}`)
  }

  if (providerTable[provider.providerNameVersionId]) {
    throw Error(`Cannot add provider that has already been added by the following providerNameVersionId: ${provider.providerNameVersionId}`)
  }

  // Record provider in providerTable
  providerTable[provider.providerNameVersionId] = provider

  if (!providerMultiplexerTable[provider.providerType]) {
    providerMultiplexerTable[provider.providerType] = new ProviderMultiplexer()
  }

  // Record provider providerNameVersionId in providerTypeToProviderListTable
  if (!providerMultiplexerTable[provider.providerType].providerNameVersionIdTable[provider.providerNameVersionId]) {
    providerMultiplexerTable[provider.providerType].providerNameVersionIdList.push(provider.providerNameVersionId)
  }
  // Record provider providerNameVersionId in providerTypeToProviderMap
  providerMultiplexerTable[provider.providerType].providerNameVersionIdTable[provider.providerNameVersionId] = true

  if (!provider.functionTable) { provider.functionTable = {} }

  // Initialize the provider by calling initializeProvider
  if (shouldCallInitializeProvider && provider.functionTable.initializeProvider && provider.functionTable.initializeProvider.function) {
    provider.functionTable.initializeProvider.function(providerConfigurationObject, extract)
  }

  return provider
}

const addProvider = {
  function: addProviderFunction
}

export {
  addProvider
}

