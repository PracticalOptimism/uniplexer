

import { ProjectExtract } from '../../../../../@project/data-structures/extract'
import { providerTable } from '../../../variables/provider'
import { deleteProviderCredential } from '../../provider-credential/delete-provider-credential'

async function deleteProviderFunction (providerNameVersionId: string, extract?: ProjectExtract): Promise<boolean> {
  const provider = providerTable[providerNameVersionId]

  if (!provider) { return Promise.resolve(true) }

  for (const credentialHashId in provider.providerCredentialTable) {
    if (!provider.providerCredentialTable.hasOwnProperty(credentialHashId)) { continue }
    await deleteProviderCredential.function(credentialHashId, extract)
  }

  delete providerTable[providerNameVersionId]

  return Promise.resolve(true)
}

const deleteProvider = {
  function: deleteProviderFunction
}

export {
  deleteProvider
}
