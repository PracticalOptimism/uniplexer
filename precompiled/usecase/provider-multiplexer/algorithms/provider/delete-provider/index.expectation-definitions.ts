
import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'

import { providerTable } from '../../../variables/provider'

import { initializeProviderTable, deleteProviderTable, NUMBER_OF_PROVIDERS_PER_TYPE, PROVIDER_TABLE } from '../../provider/add-provider/index.expectation-definitions'

describe(getProjectFilePath.function(__filename), () => {
  it('should remove provider from providerTable', async () => {

    // Expect there to be exactly 0 providers in the providerTable
    expect(Object.keys(providerTable).length).eq(0)

    // Add many providers to the providerTable
    initializeProviderTable()

    // Expect a certain number of providers in the providerTable
    const numberOfExpectedProviderCredentials = NUMBER_OF_PROVIDERS_PER_TYPE * Object.keys(PROVIDER_TABLE).length
    expect(Object.keys(providerTable).length).eq(numberOfExpectedProviderCredentials)

    // Remove the providers from the providerTable
    const deleteResultList = await deleteProviderTable()

    // Expect 0 providers in the providerTable
    expect(Object.keys(providerTable).length).eq(0)

    // Expect deleteProvider to return true to indicate deletion has occurred
    for (let i = 0; i < deleteResultList.length; i++) {
      expect(deleteResultList[i]).to.equal(true)
    }
  })
})

