
import { ProjectExtract } from '../../../../../@project/data-structures/extract'
import { getProviderList } from '../get-provider-list'
import { Provider } from '../../../data-structures/provider'


function getRandomProviderFunction (providerType: string, operatingEnvironmentId?: string, _extract?: ProjectExtract): Provider {
  let providerList = getProviderList.function(providerType)
  if (operatingEnvironmentId) {
    providerList = providerList.filter((provider: Provider) => {
      if (!provider.operatingSystemAndOpenAccessHardwareSpecificationsForPresentComputerOperatingSystem) { return false }
      return provider.operatingSystemAndOpenAccessHardwareSpecificationsForPresentComputerOperatingSystem[operatingEnvironmentId] ? true : false
    })
  }
  return providerList[Math.floor(Math.random() * providerList.length)]
}

const getRandomProvider = {
  function: getRandomProviderFunction
}

export {
  getRandomProvider
}
