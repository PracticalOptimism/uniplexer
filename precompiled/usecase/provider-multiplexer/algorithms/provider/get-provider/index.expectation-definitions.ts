
import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'

import { getProvider } from '.'

import { initializeProviderTable, deleteProviderTable, NUMBER_OF_PROVIDERS_PER_TYPE, PROVIDER_TABLE } from '../add-provider/index.expectation-definitions'

describe(getProjectFilePath.function(__filename), () => {
  before(() => {
    initializeProviderTable()
  })
  after(async () => {
    await deleteProviderTable()
  })
  it('should get provider from providerTable', async () => {
    for (const providerType in PROVIDER_TABLE) {
      if (!PROVIDER_TABLE.hasOwnProperty(providerType)) { continue }
      for (let i = 0; i < NUMBER_OF_PROVIDERS_PER_TYPE; i++) {
        const providerNameVersionId = `mock-${providerType}@${i}.0.0`

        // Get provider
        const provider = getProvider.function(providerNameVersionId)

        // Expect the retrieved provider to have the same providerNameVersionId as the requested providerNameVersionId
        expect(provider.providerNameVersionId).to.equal(providerNameVersionId)
      }
    }
  })
})

