
import { ProjectExtract } from '../../../../../@project/data-structures/extract'
import { Provider } from '../../../data-structures/provider'
import { providerTable } from '../../../variables/provider'

function getProviderFunction (providerNameVersionId: string, _extract?: ProjectExtract): Provider {
  return providerTable[providerNameVersionId]
}

const getProvider = {
  function: getProviderFunction
}

export {
  getProvider
}
