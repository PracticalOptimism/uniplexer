

import { defaultProviderTable } from '../../../variables/provider'


function initializeDefaultProviderFunction (providerType: string, providerNameVersionId: string) {
  defaultProviderTable[providerType] = providerNameVersionId
}

const initializeDefaultProvider = {
  function: initializeDefaultProviderFunction
}

export {
  initializeDefaultProvider
}

