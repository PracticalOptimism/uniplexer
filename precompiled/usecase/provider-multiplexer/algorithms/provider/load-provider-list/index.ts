


function loadProviderListFunction () {
  // TODO: load provider list from database
  //
  // QUESTION [Date: May 13, 2020]:
  // Can you save functions to jason database?
    // HYPOTHESIS: Because it is not allowed to save
    // JSON that contains functions, saving a provider
    // and then loading it will not preserve the functionTable
    // therefore, perhaps only allow adding provider and
    // don't save the provider to a database. .
}

const loadProviderList = {
  function: loadProviderListFunction
}

export {
  loadProviderList
}
