
import { providerMultiplexerTable } from '../../../variables/provider-multiplexer'

function deleteProviderMultiplexerFunction (providerType: string): boolean {
  delete providerMultiplexerTable[providerType]
  return true
}

const deleteProviderMultiplexer = {
  function: deleteProviderMultiplexerFunction
}

export {
  deleteProviderMultiplexer
}

