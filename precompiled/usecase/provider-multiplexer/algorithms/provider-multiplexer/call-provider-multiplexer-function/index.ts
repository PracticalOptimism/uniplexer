
import { ProjectExtract } from '../../../../../@project/data-structures/extract'
import { providerTable, providerCredentialTable } from '../../../variables/provider'
import { getProviderList } from '../../provider/get-provider-list'
import { providerMultiplexerTable } from '../../../variables/provider-multiplexer'

async function callProviderMultiplexerFunctionFunction (providerType: string, functionName: string, inputList: any[], extract?: ProjectExtract): Promise<any[]> {
  let providerNameVersionIdList: string[] = []

  let doesHaveExtractProviderCredentialTable: boolean = false
  let extractProviderCredentialTable: { [providerCredentialId: string]: boolean } = {}

  const resultList: any[] = []

  // Retrieve the provider credential id table or provider name version id table
  // to select specific providers for this function call
  if (extract && extract.usecase && extract.usecase.providerMultiplexer) {
    // Retrieve the provider name version id to know which providers to select for this function call
    if (extract.usecase.providerMultiplexer.providerTable && extract.usecase.providerMultiplexer.providerTable[providerType]) {
      providerNameVersionIdList = providerNameVersionIdList.concat(Object.keys(extract.usecase.providerMultiplexer.providerTable[providerType]))
    }

    // Retrieve the provider credential id to know which providers to select for this function call
    if (extract.usecase.providerMultiplexer.providerCredentialTable) {
      doesHaveExtractProviderCredentialTable = extract.usecase.providerMultiplexer.providerCredentialTable ? true : false
      extractProviderCredentialTable = extract.usecase.providerMultiplexer.providerCredentialTable
    }
  }

  // If there are credentials, use the providers that satisfy those credentials
  if (doesHaveExtractProviderCredentialTable) {
    for (let providerCredentialId in extractProviderCredentialTable) {
      if (!extractProviderCredentialTable.hasOwnProperty(providerCredentialId)) { continue }
      const providerCredential = providerCredentialTable[providerCredentialId]
      providerNameVersionIdList.push(providerCredential.providerCredentialConfigurationSettingsObject.providerNameVersionId)
    }
  } else if (providerMultiplexerTable[providerType]) {
    // If there are no credentials, use a random provider of the given type
    const providerList = getProviderList.function(providerType)
    const randomProvider = providerList[Math.floor(Math.random() * providerList.length)]
    if (randomProvider) {
      providerNameVersionIdList.push(randomProvider.providerNameVersionId)
    }
  }

  // Call the provider functions with the provided input and add output to result list
  for (let i = 0; i < providerNameVersionIdList.length; i++) {
    const providerNameVersionId = providerNameVersionIdList[i]
    const provider = providerTable[providerNameVersionId]
    if (!provider || !provider.functionTable || !provider.functionTable[functionName] || !provider.functionTable[functionName].function) { continue }
    resultList.push(await provider.functionTable[functionName].function(...inputList))
  }

  return resultList.flat()
}

const callProviderMultiplexerFunction = {
  function: callProviderMultiplexerFunctionFunction
}

export {
  callProviderMultiplexerFunction
}

