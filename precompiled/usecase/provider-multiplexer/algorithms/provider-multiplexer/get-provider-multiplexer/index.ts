
import { ProjectExtract } from '../../../../../@project/data-structures/extract'
import { providerMultiplexerTable } from '../../../variables/provider-multiplexer'

function getProviderMultiplexerFunction (_extract?: ProjectExtract) {
  return providerMultiplexerTable
}

const getProviderMultiplexer = {
  function: getProviderMultiplexerFunction
}

export {
  getProviderMultiplexer
}

