import { providerMultiplexerTable, providerMultiplexerFunctionTableMap } from '../../../variables/provider-multiplexer'

import { ProviderMultiplexer } from '../../../data-structures/provider-multiplexer'

function addProviderMultiplexerFunction (providerType: string, providerMultiplexerFunctionTable: { [functionName: string]: any }): ProviderMultiplexer<any> {
  if (providerMultiplexerTable[providerType]) {
    throw Error(`Cannot add existing multiplex provider type ${providerType}`)
  }

  const providerMultiplexer = new ProviderMultiplexer<any>()
  providerMultiplexer.providerType = providerType
  providerMultiplexer.providerMultiplexerFunctionTable = providerMultiplexerFunctionTable || {}

  providerMultiplexerTable[providerType] = providerMultiplexer
  providerMultiplexerFunctionTableMap[providerType] = providerMultiplexerFunctionTable
  return providerMultiplexer
}

const addProviderMultiplexer = {
  function: addProviderMultiplexerFunction
}

export {
  addProviderMultiplexer
}

