

import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'

import { initializeProviderMultiplexerTable } from './index'
import { providerMultiplexerTable } from '../../../variables/provider-multiplexer'

import { PROVIDER_TYPE_LIST } from '../../../constants/provider-type'

describe(getProjectFilePath.function(__filename), () => {
  it('should initialize providerMultiplexerTypeTable', () => {

    // Initialize provider multiplexer type table
    initializeProviderMultiplexerTable.function()

    // Expect the providerMultiplexerTypeTable to reference each of the default provider types
    for (let i = 0; i < PROVIDER_TYPE_LIST.length; i++) {
      const providerType = PROVIDER_TYPE_LIST[i]
      expect(providerMultiplexerTable[providerType]).to.not.equal(null || undefined)
    }
  })
})

