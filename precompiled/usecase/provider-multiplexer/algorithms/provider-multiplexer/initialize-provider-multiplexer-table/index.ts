
import { ProjectExtract } from '../../../../../@project/data-structures/extract'
import { PROVIDER_TYPE_LIST } from '../../../constants/provider-type'
import { providerMultiplexerTable, providerMultiplexerFunctionTableMap } from '../../../variables/provider-multiplexer'

import * as providerMultiplexerConstants from '../../../constants/provider-multiplexer'
import { FUNCTION_NAME_LIST as PROVIDER_FUNCTION_NAME_LIST } from '../../../constants/provider/provider-function'

import { callProviderMultiplexerFunction } from '../call-provider-multiplexer-function'
import { ProviderMultiplexer } from '../../../data-structures/provider-multiplexer'


function initializeProviderMultiplexerTableFunction (extract?: ProjectExtract) {
  for (let providerTypeListIndex = 0; providerTypeListIndex < PROVIDER_TYPE_LIST.length; providerTypeListIndex++) {
    const providerType = PROVIDER_TYPE_LIST[providerTypeListIndex]

    const providerMultiplexer = new ProviderMultiplexer<any>()
    providerMultiplexer.providerType = providerType
    providerMultiplexer.providerMultiplexerFunctionTable = {}
    providerMultiplexerFunctionTableMap[providerType] = {}
    providerMultiplexerTable[providerType] = providerMultiplexer

    const providerTypeConstantsTable: { [constantName: string]: any } = (providerMultiplexerConstants as any)[providerType]

    if (!providerTypeConstantsTable) { continue }

    const functionNameList: string[] = (providerTypeConstantsTable.FUNCTION_NAME_LIST as string[]).concat(PROVIDER_FUNCTION_NAME_LIST)

    for (let functionNameListIndex = 0; functionNameListIndex < functionNameList.length; functionNameListIndex++) {
      const functionName = functionNameList[functionNameListIndex]
      const functionAlgorithm = {
        function: async function () {
          return callProviderMultiplexerFunction.function(providerType, functionName, [...arguments], extract)
        }
      }
      providerMultiplexer.providerMultiplexerFunctionTable[functionName] = functionAlgorithm
      providerMultiplexerFunctionTableMap[providerType][functionName] = functionAlgorithm
    }
  }
}

const initializeProviderMultiplexerTable = {
  function: initializeProviderMultiplexerTableFunction
}

export {
  initializeProviderMultiplexerTable
}

