
import { ProjectExtract } from '../../../../../@project/data-structures/extract'
import { providerMultiplexerFunctionTableMap } from '../../../variables/provider-multiplexer'

function getProviderMultiplexerFunctionTableFunction (_extract?: ProjectExtract) {
  return providerMultiplexerFunctionTableMap
}

const getProviderMultiplexerFunctionTable = {
  function: getProviderMultiplexerFunctionTableFunction
}

export {
  getProviderMultiplexerFunctionTable
}

