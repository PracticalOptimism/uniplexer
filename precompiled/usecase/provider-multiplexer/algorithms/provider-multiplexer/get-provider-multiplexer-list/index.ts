

import { providerMultiplexerTable } from '../../../variables/provider-multiplexer'
import { ProviderMultiplexer } from '../../../data-structures/provider-multiplexer'

function getProviderMultiplexerListFunction (): ProviderMultiplexer<any>[] {
  return Object.keys(providerMultiplexerTable).map((providerType: string) => providerMultiplexerTable[providerType])
}

const getProviderMultiplexerList = {
  function: getProviderMultiplexerListFunction
}

export {
  getProviderMultiplexerList
}

