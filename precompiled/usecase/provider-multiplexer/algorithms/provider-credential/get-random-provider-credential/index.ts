import { getProviderCredentialList } from '../get-provider-credential-list'
import { ProviderCredential } from '../../../data-structures/provider-credential'


function getRandomProviderCredentialFunction (providerType: string): ProviderCredential {
  const providerCredentialList = getProviderCredentialList.function(providerType)
  return providerCredentialList[Math.floor(Math.random() * providerCredentialList.length)]
}

const getRandomProviderCredential = {
  function: getRandomProviderCredentialFunction
}

export {
  getRandomProviderCredential
}
