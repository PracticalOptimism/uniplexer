
import { ProjectExtract } from '../../../../../@project/data-structures/extract'
import { ProviderCredential } from '../../../data-structures/provider-credential'
import { getProviderMultiplexerFunctionTable } from '../../provider-multiplexer/get-provider-multiplexer-function-table'

import { PROVIDER_CREDENTIAL_DATASTORE } from '../../../constants/provider-credential'

import { providerTable, providerCredentialTable } from '../../../variables/provider'
import { ProviderMultiplexer } from '../../../data-structures/provider-multiplexer'
import { providerMultiplexerTable } from '../../../variables/provider-multiplexer'

async function loadProviderCredentialListFunction (extract?: ProjectExtract): Promise<ProviderCredential[]> {

  const { database } = getProviderMultiplexerFunctionTable.function(extract)
  let providerCredentialList: ProviderCredential[] = []

  // Load the provider credential list from the database
  try {
    providerCredentialList = (await database!.getItemList!.function(PROVIDER_CREDENTIAL_DATASTORE, extract))[0].providerFunctionResponse
  } catch (errorMessage) {
    throw Error(`Error while retrieving the provider credential datastore for the provider credential list : ${extract}. The database responded with: ${errorMessage}`)
  }

  for (let i = 0; i < providerCredentialList.length; i++) {
    const providerCredential = providerCredentialList[i]
    providerCredentialTable[providerCredential.providerCredentialId] = providerCredential

    // Create the provider type of the provider credentials' providerNameVersionId
    const provider = providerTable[providerCredential.providerCredentialConfigurationSettingsObject.providerNameVersionId]
    let providerMultiplexer = providerMultiplexerTable[provider.providerType]
    if (!providerMultiplexer) {
      providerMultiplexer = new ProviderMultiplexer<any>()
      providerMultiplexer.providerMultiplexerFunctionTable = {}
    }

    providerMultiplexer.providerCredentialIdTable[providerCredential.providerCredentialId] = true
    if (!providerMultiplexer.providerCredentialIdTable[provider.providerNameVersionId]) {
      providerMultiplexer.providerNameVersionIdList.push(provider.providerNameVersionId)
    }
    providerMultiplexer.providerCredentialIdTable[provider.providerNameVersionId] = true

    // Add the provider type to the provider type table
    providerMultiplexer[provider.providerType] = providerMultiplexer
  }

  // return the providerCredentialList
  return providerCredentialList
}

const loadProviderCredentialList = {
  function: loadProviderCredentialListFunction
}

export {
  loadProviderCredentialList
}
