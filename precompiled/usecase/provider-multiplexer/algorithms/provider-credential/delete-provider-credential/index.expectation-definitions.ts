
import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'

import { providerCredentialTable } from '../../../variables/provider'

import { initializeProviderTable, deleteProviderTable, NUMBER_OF_PROVIDERS_PER_TYPE } from '../../provider/add-provider/index.expectation-definitions'
import { initializeProviderCredentialTable, deleteProviderCredentialTable, PROVIDER_CREDENTIAL_TABLE } from '../add-provider-credential/index.expectation-definitions'


describe(getProjectFilePath.function(__filename), () => {
  before(async () => {
    initializeProviderTable()
  })
  after(async () => {
    await deleteProviderTable()
  })
  it('should remove providerCredential from providerCredentialTable', async () => {

    // Add provider credentials
    await initializeProviderCredentialTable()

    // Expect a certain number of provider credentials to exist in providerCredentialTable
    const numberOfExpectedProviderCredentials = NUMBER_OF_PROVIDERS_PER_TYPE * Object.keys(PROVIDER_CREDENTIAL_TABLE).length
    expect(Object.keys(providerCredentialTable).length).to.equal(numberOfExpectedProviderCredentials)

    // Delete all the provider credentials
    const deleteResultList = await deleteProviderCredentialTable()

    // Expect 0 credentials to exist in providerCredentialTable
    expect(Object.keys(providerCredentialTable).length).to.equal(0)

    // Expect deleteProviderCredential to return true to indicate that a delete has occurred
    for (let i = 0; i < deleteResultList.length; i++) {
      expect(deleteResultList[i]).to.equal(true)
    }
  })
})

