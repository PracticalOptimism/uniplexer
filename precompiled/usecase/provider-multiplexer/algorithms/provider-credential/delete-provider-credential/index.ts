
import { ProjectExtract } from '../../../../../@project/data-structures/extract'
import { PROVIDER_CREDENTIAL_DATASTORE } from '../../../constants/provider-credential'

import { providerTable, providerCredentialTable, defaultProviderTable } from '../../../variables/provider'
import { getProviderMultiplexerFunctionTable } from '../../provider-multiplexer/get-provider-multiplexer-function-table'
import { getProvider } from '../../provider/get-provider'
import { PROVIDER_TYPE_DATABASE } from '../../../constants/provider-type'
import { updateObjectValue } from '../../../../../@utility/algorithms/update-object-value'

async function deleteProviderCredentialFunction (credentialHashId: string, extract?: ProjectExtract): Promise<boolean> {

  const providerCredential = providerCredentialTable[credentialHashId]

  if (!providerCredential) { return true }

  const provider = providerTable[providerCredential.providerCredentialConfigurationSettingsObject.providerNameVersionId]

  delete provider.providerCredentialTable![credentialHashId]
  delete providerCredentialTable[credentialHashId]

  const { database } = getProviderMultiplexerFunctionTable.function(extract)
  let isDeleted = false

  // Retrieve default database provider
  const defaultDatabaseProvider = getProvider.function(defaultProviderTable[PROVIDER_TYPE_DATABASE])

  // Remove a database record for the provider credential
  if (defaultDatabaseProvider) {

    // Add the provider name version id to the extract object to have a default database to store provider credentials
    updateObjectValue.function(extract, ['usecase', 'providerMultiplexer', 'providerTable', defaultDatabaseProvider.providerType, defaultDatabaseProvider.providerNameVersionId], true)

    // Remove the provider credential from the database
    try {
      isDeleted = (await database!.deleteItem!.function(PROVIDER_CREDENTIAL_DATASTORE, credentialHashId))[0].providerFunctionResponse || false
    } catch (errorMessage) {
      throw Error(`Cannot delete provider credential from database for the following reason: ${errorMessage}`)
    }
  }

  return isDeleted
}

const deleteProviderCredential = {
  function: deleteProviderCredentialFunction
}

export {
  deleteProviderCredential
}
