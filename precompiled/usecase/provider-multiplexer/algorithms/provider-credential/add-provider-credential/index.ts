
import { ProjectExtract } from '../../../../../@project/data-structures/extract'
import { ProviderCredential, ProviderCredentialConfigurationSettingsObject } from '../../../data-structures/provider-credential'
import { providerCredentialTable, defaultProviderTable } from '../../../variables/provider'
import { PROVIDER_CREDENTIAL_DATASTORE } from '../../../constants/provider-credential'
import { getProvider } from '../../provider/get-provider'
import { getProviderMultiplexerFunctionTable } from '../../provider-multiplexer/get-provider-multiplexer-function-table'
import { getProviderList } from '../../provider/get-provider-list'
import { PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION, PROVIDER_TYPE_DATABASE } from '../../../constants/provider-type'
import { FUNCTION_NAME_CREATE_SHA1_CRYPTOGRAPHIC_HASH_VALUE } from '../../../constants/provider-multiplexer/cryptographic-hash-function'
import { FUNCTION_NAME_CREATE_ITEM } from '../../../constants/provider-multiplexer/database'
import { updateObjectValue } from '../../../../../@utility/algorithms/update-object-value'

async function addProviderCredentialFunction (configurationSettingsObject: ProviderCredentialConfigurationSettingsObject, extract?: ProjectExtract): Promise<ProviderCredential> {

  const provider = getProvider.function(configurationSettingsObject.providerNameVersionId)

  if (!provider) {
    throw Error(`Error calling addProviderCredential(): Cannot create provider credential for a provider that isn't added. Please add the provider with the providerNameVersionId provided before adding credentials for that provider.`)
  }

  const { cryptographicHashFunction, database } = getProviderMultiplexerFunctionTable.function(extract)

  console.log('Number of Cryptographic Hash Function Providers: ', getProviderList.function('cryptographicHashFunction').length)

  // Retrieve the default cryptographic hash function provider
  const defaultCryptographicHashFunctionProvider = getProvider.function(defaultProviderTable[PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION])
  if (!defaultCryptographicHashFunctionProvider) {
    throw Error(`Cannot create provider credential without a default cryptographic hash function provider`)
  }

  // Add the provider name version id to the extract object to have a default database to store provider credentials
  extract = updateObjectValue.function(extract, ['usecase', 'providerMultiplexer', 'providerTable', defaultCryptographicHashFunctionProvider.providerType, defaultCryptographicHashFunctionProvider.providerNameVersionId], true)

  // Create cryptographic hash value for the provider credential
  let cryptographicHashValue
  try {
    cryptographicHashValue = (await cryptographicHashFunction!.createSha1CryptographicHashValue!.function(configurationSettingsObject, extract))[0].providerFunctionResponse
  } catch (errorMessage) {
    throw Error(`Cannot create cryptographic hash value for provider credential configuration data: ${JSON.stringify(configurationSettingsObject)}. Error message: ${errorMessage}`)
  }

  // Ensure there is a cryptographic hash value
  if (!cryptographicHashValue) {
    throw Error(`Cryptographic hash value is left null or undefined and cannot be used for referencing a provider credential. Please ensure a cryptographic hash function provider is initialized with this project application using the addProvider() function.`)
  }

  // Ensure credential hasn't already been added
  if (providerCredentialTable[cryptographicHashValue]) {
    return providerCredentialTable[cryptographicHashValue]
  }

  // Create provider credential
  const providerCredential = new ProviderCredential()
  providerCredential.providerCredentialId = cryptographicHashValue
  providerCredential.providerCredentialConfigurationSettingsObject = configurationSettingsObject
  providerCredential.providerDependencyTable[PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION] = {
    [defaultCryptographicHashFunctionProvider.providerNameVersionId]: { [FUNCTION_NAME_CREATE_SHA1_CRYPTOGRAPHIC_HASH_VALUE]: true }
  }

  // Retrieve default database provider
  const defaultDatabaseProvider = getProvider.function(defaultProviderTable[PROVIDER_TYPE_DATABASE])

  // Create a database record for the provider credential
  if (defaultDatabaseProvider) {
    providerCredential.providerDependencyTable[PROVIDER_TYPE_DATABASE] = {
      [defaultDatabaseProvider.providerNameVersionId]: { [FUNCTION_NAME_CREATE_ITEM]: true }
    }

    // Add the provider name version id to the extract object to have a default database to store provider credentials
    updateObjectValue.function(extract, ['usecase', 'providerMultiplexer', 'providerTable', defaultDatabaseProvider.providerType, defaultDatabaseProvider.providerNameVersionId], true)

    // Store the provider credential in the database
    try {
      await database!.createItem!.function(PROVIDER_CREDENTIAL_DATASTORE, providerCredential, providerCredential.providerCredentialId, extract)
    } catch (errorMessage) {
      throw Error(`Cannot create provider credential item in database due to following error response from database provider: ${errorMessage}`)
    }
  }

  // Save credential to credentialTable and the provider's credential table
  providerCredentialTable[providerCredential.providerCredentialId] = providerCredential
  provider.providerCredentialTable![providerCredential.providerCredentialId] = providerCredential

  return providerCredential
}

const addProviderCredential = {
  function: addProviderCredentialFunction
}

export {
  addProviderCredential
}
