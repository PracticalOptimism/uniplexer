
import 'mocha'
import { expect } from 'chai'
import { getProjectFilePath } from '../../../../../@expectation-definitions/algorithms/get-project-file-path'

import { providerCredentialTable } from '../../../variables/provider'
import { addProviderCredential } from './index'
import { deleteProviderCredential } from '../delete-provider-credential'

import { initializeProviderTable, deleteProviderTable, NUMBER_OF_PROVIDERS_PER_TYPE } from '../../provider/add-provider/index.expectation-definitions'

import { ProviderCredential, ProviderCredentialConfigurationSettingsObject } from '../../../data-structures/provider-credential'

import { PROVIDER_TYPE_ASYMMETRIC_CRYPTOGRAPHY, PROVIDER_TYPE_DATABASE, PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION } from '../../../constants/provider-type'

import { initializeProviderMultiplexerTable } from '../../provider-multiplexer/initialize-provider-multiplexer-table'


const PROVIDER_CREDENTIAL_TABLE: { [providerType: string]: ProviderCredential[] } = {
  [PROVIDER_TYPE_ASYMMETRIC_CRYPTOGRAPHY]: [],
  [PROVIDER_TYPE_DATABASE]: [],
  [PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION]: []
}

async function initializeProviderCredentialTable () {
  for (const providerType in PROVIDER_CREDENTIAL_TABLE) {
    if (!PROVIDER_CREDENTIAL_TABLE.hasOwnProperty(providerType)) { continue }

    // Add provider credentials for each of the providers of this given provider type
    for (let i = 0; i < NUMBER_OF_PROVIDERS_PER_TYPE; i++) {
      const configurationObject = new ProviderCredentialConfigurationSettingsObject()
      configurationObject.providerNameVersionId = `mock-${providerType}@${i}.0.0`
      const credential = await addProviderCredential.function(configurationObject)
      PROVIDER_CREDENTIAL_TABLE[providerType].push(credential)
    }
  }
}

async function deleteProviderCredentialTable (): Promise<boolean[]> {
  const deleteResultList = []
  for (const providerType in PROVIDER_CREDENTIAL_TABLE) {
    if (!PROVIDER_CREDENTIAL_TABLE.hasOwnProperty(providerType)) { continue }

    // Delete the provider credential for each of the providers of this given provider type
    for (let i = 0; i < NUMBER_OF_PROVIDERS_PER_TYPE; i++) {
      const deleteResult = await deleteProviderCredential.function(PROVIDER_CREDENTIAL_TABLE[providerType][i].providerCredentialId)
      deleteResultList.push(deleteResult)
    }
    PROVIDER_CREDENTIAL_TABLE[providerType] = []
  }
  return deleteResultList
}

describe(getProjectFilePath.function(__filename), () => {
  before(() => {
    initializeProviderMultiplexerTable.function()
    initializeProviderTable()
  })
  after(async () => {
    await deleteProviderTable()
  })
  it('should add providerCredential to providerCredentialTable', async () => {

    // Expect there to be exactly 0 provider credentials
    expect(Object.keys(providerCredentialTable).length).eq(0)

    // Add provider credentials
    await initializeProviderCredentialTable()

    // Expect there to be exactly 3 provider credentials
    const numberOfExpectedProviderCredentials = NUMBER_OF_PROVIDERS_PER_TYPE * Object.keys(PROVIDER_CREDENTIAL_TABLE).length
    expect(Object.keys(providerCredentialTable).length).eq(numberOfExpectedProviderCredentials)

    // Expect credential hash id to not be undefined
    for (const providerType in PROVIDER_CREDENTIAL_TABLE) {
      if (!PROVIDER_CREDENTIAL_TABLE.hasOwnProperty(providerType)) { continue }
      for (let i = 0; i < NUMBER_OF_PROVIDERS_PER_TYPE; i++) {
        expect(PROVIDER_CREDENTIAL_TABLE[providerType][i].providerCredentialId).to.not.eq(undefined)
      }
    }

    // Remove provider credentials
    await deleteProviderCredentialTable()

    expect(Object.keys(providerCredentialTable).length).to.equal(0)
  })
})

export {
  PROVIDER_CREDENTIAL_TABLE,
  initializeProviderCredentialTable,
  deleteProviderCredentialTable
}

