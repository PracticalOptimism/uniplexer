
import { ProjectExtract } from '../../../../../@project/data-structures/extract'
import { providerTable, providerCredentialTable } from '../../../variables/provider'
import { ProviderCredential } from '../../../data-structures/provider-credential'
import { providerMultiplexerTable } from '../../../variables/provider-multiplexer'

function getProviderCredentialListFunction (providerType?: string, _extract?: ProjectExtract): ProviderCredential[] {

  if (!providerType) {
    return Object.keys(providerCredentialTable).map((credentialHashId: string) => providerCredentialTable[credentialHashId])
  }

  const providerNameVersionIdList = providerMultiplexerTable[providerType].providerNameVersionIdList

  let providerCredentialList: string[] = []

  for (let i = 0; i < providerNameVersionIdList.length; i++) {
    const credentialTable = providerTable[providerNameVersionIdList[i]].providerCredentialTable || {}
    providerCredentialList = providerCredentialList.concat(Object.keys(credentialTable))
  }

  return providerCredentialList.map((credentialHashId: string) => providerCredentialTable[credentialHashId])
}

const getProviderCredentialList = {
  function: getProviderCredentialListFunction
}

export {
  getProviderCredentialList
}
