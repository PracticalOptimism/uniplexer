
import { ProjectExtract } from '../../../../../@project/data-structures/extract'
import { providerCredentialTable } from '../../../variables/provider'
import { ProviderCredential } from '../../../data-structures/provider-credential'

function getProviderCredentialFunction (credentialHashId: string, _extract?: ProjectExtract): ProviderCredential {
  return providerCredentialTable[credentialHashId]
}

const getProviderCredential = {
  function: getProviderCredentialFunction
}

export {
  getProviderCredential
}
