
import { ProjectExtract } from '../../../../@project/data-structures/extract'

declare type InitializeProvider = (configurationSettingsObject?: any, extract?: ProjectExtract) => InitializeProviderFunctionResponse
declare type InitializeProviderAlgorithm = { function: InitializeProvider }
declare type InitializeProviderFunctionResponse = Promise<ProviderFunctionResponse<any, ProviderAdditionalResponseInformationTable>[]>

interface ProviderAdditionalResponseInformationTable { [providerInformationId: string]: any }

class ProviderFunctionResponse <ResponseType, AdditionalResponseInformationTableType> {
  providerFunctionResponse?: ResponseType
  providerFunctionResponseType?: any

  providerManufacturerId?: string = ''
  providerNameVersionId?: string = ''
  providerCredentialId?: string = ''
  providerAdditionalResponseInformationTable?: AdditionalResponseInformationTableType

  constructor (providerFunction?: ProviderFunctionResponse<ResponseType, AdditionalResponseInformationTableType>) {
    Object.assign(this, providerFunction)
  }
}

export {
  InitializeProvider,
  InitializeProviderAlgorithm,
  InitializeProviderFunctionResponse,

  ProviderFunctionResponse
}
