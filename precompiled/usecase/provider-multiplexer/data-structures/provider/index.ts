
import { ProviderCredential } from '../provider-credential'

class Provider {
  providerNameVersionId: string = ''
  providerType: string = ''

  providerName?: string = ''

  providerDefaultConfigurationSettingsObject?: any
  hasOfflineSupport?: boolean = false
  isMockProvider?: boolean = false

  websiteUrlTable?: { [websiteUrl: string]: ProviderWebsiterUrl } = {}

  iconImageUrlTable?: { [iconImageUrl: string]: ProviderIconImageUrl } = {}
  imageScreenShotUrlTable?: { [imageScreenShotUrl: string]: ProviderImageScreenShotUrl } = {}

  textDescriptionTable?: { [textDescriptionId: string ]: ProviderTextDescription } = {}

  providerImport?: any

  operatingSystemAndOpenAccessHardwareSpecificationsForPresentComputerOperatingSystem?: { [operatingEnvironmentId: string]: ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds } = {}

  providerCredentialTable?: { [providerCredentialId: string]: ProviderCredential } = {}

  functionTable?: { [functionName: string]: any } = {}

  constructor (defaultProvider?: Provider) {
    Object.assign(this, defaultProvider)
  }
}

class ProviderWebsiterUrl {
  websiteUrl: string = ''

  providerNameVersionId?: string = ''

  websiteName?: string = ''
  websiteDescription?: string = ''

  constructor (defaultProviderWebsiteUrl?: ProviderWebsiterUrl) {
    Object.assign(this, defaultProviderWebsiteUrl)
  }
}

class ProviderIconImageUrl {
  iconImageUrl: string = ''

  providerNameVersionId?: string = ''
  iconImagePlaceholderText?: string = ''

  constructor (defaultProviderIconImageUrl?: ProviderIconImageUrl) {
    Object.assign(this, defaultProviderIconImageUrl)
  }
}

class ProviderImageScreenShotUrl {
  imageScreenShotUrl: string = ''

  providerNameVersionId?: string = ''
  imageScreenShotPlaceholderText?: string = ''

  constructor (defaultProviderImageScreenShotUrl?: ProviderImageScreenShotUrl) {
    Object.assign(this, defaultProviderImageScreenShotUrl)
  }
}

class ProviderTextDescription {
  textDescriptionId: string = ''

  nameOfSourceForText: string = ''
  nameOfAuthorForText: string = ''
  nameOfOriginalPublisherForText?: string = ''
  nameOfOriginalPublisherCoordinatorForText?: string = ''
  nameOfOriginalPostOfficeSendingMethodForText?: string = ''
  nameOfPublicOfficeWherePostMessageWasSentFrom?: string = ''
  nameOfPostOfficeMethodOriginallyPublishedBySentSubscriberSignalMessageUploadMethod?: string = ''

  constructor (defaultProviderTextDescription?: ProviderTextDescription) {
    Object.assign(this, defaultProviderTextDescription)
  }
}

class ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds {
  presentOperatingEnvironmentId?: string = ''
  presentOperatingSystemId?: string = ''
  presentOperatingSystemName?: string = ''
  presentOperatingSystemDevelopmentPlan?: string = ''
  presentOperatingSystemHardDriveSpaceRequirements?: PresentOperatingSystemHardDriveRequirements = new PresentOperatingSystemHardDriveRequirements()

  constructor (defaultProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds?: ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds) {
    Object.assign(this, defaultProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds)
  }
}

class PresentOperatingSystemHardDriveRequirements {
  presentOperatingSystemHardDriveRequirementId: string = ''

  openAccessNetworkToAcquireNeutralConsensusDataOnTheFirmwareOfTheDeviceSpecifications: PresentFirmwareManufacturerId = new PresentFirmwareManufacturerId()

  constructor (defaultPresentOperatingSystemHardDriveRequirements?: PresentOperatingSystemHardDriveRequirements) {
    Object.assign(this, defaultPresentOperatingSystemHardDriveRequirements)
  }
}

class PresentFirmwareManufacturerId {
  openAcessOpportunityToEnterTheDomain: string = ''

  constructor (defaultPresentFirmwareManufacturerId?: PresentFirmwareManufacturerId) {
    Object.assign(this, defaultPresentFirmwareManufacturerId)
  }
}

export {
  Provider,
  ProviderWebsiterUrl,
  ProviderIconImageUrl,
  ProviderImageScreenShotUrl,
  ProviderTextDescription,

  ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds,
  PresentOperatingSystemHardDriveRequirements,
  PresentFirmwareManufacturerId
}

