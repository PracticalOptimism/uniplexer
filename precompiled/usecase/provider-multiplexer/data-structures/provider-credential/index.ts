

class ProviderCredential {
  providerCredentialId: string = ''

  configurationDataSha1CryptographicHash: string = ''
  providerCredentialConfigurationSettingsObject: ProviderCredentialConfigurationSettingsObject = new ProviderCredentialConfigurationSettingsObject()

  // providerDependencyTable: { [providerNameVersionId: string]: boolean } = {}
  providerDependencyTable: { [providerType: string]: { [providerNameVersionId: string]: { [providerFunctionName: string]: boolean } } } = {}
  // providerCredentialTable: { [providerCredentialId: string]: boolean } = {} // for the future for other data types

  constructor (defaultProviderCredential?: ProviderCredential) {
    Object.assign(this, defaultProviderCredential)
  }
}

class ProviderCredentialConfigurationSettingsObject {
  providerNameVersionId: string = ''
  configurationSettingsTable: any = {}

  constructor (defaultProviderCredentialConfigurationSettingsObject?: ProviderCredentialConfigurationSettingsObject) {
    Object.assign(this, defaultProviderCredentialConfigurationSettingsObject)
  }
}

export {
  ProviderCredential,
  ProviderCredentialConfigurationSettingsObject
}

