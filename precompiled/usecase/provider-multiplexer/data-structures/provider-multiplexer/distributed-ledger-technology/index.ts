

// import { ProjectExtract } from '../../../../../@project/data-structures/extract'
// import { InitializeProvider } from '../provider'

// interface DistributedLedgerTechnologyProvider {
//   initializeProvider: InitializeProvider
//   getState (stateId: string): Promise<any>
//   updateState (stateId: string, value: any): Promise<any>
//   addStateListener (stateId: string, callback: (state: any) => void): Promise<any>
//   removeStateListener (stateId: string, listenerId: string): Promise<any>
//   setSyncFrequency (syncTickRate: number): Promise<any>
//   setPeerCommunicationCallbacks (discoveryAndConnectionCallback: DiscoConnectionCallback, sendMessageCallback: SendMessageCallback, receiveMessageCallback: ReceiveMessageCallback): void
// }

// declare type DiscoConnectionCallback = (clientCredential: any) => void
// declare type SendMessageCallback = (message: any) => void
// declare type ReceiveMessageCallback = () => any

// interface DistributedLedgerTechnologyMultiplexer {
//   initializeProvider: InitializeProvider
//   getState (stateId: string, extract?: ProjectExtract): Promise<any[]>
//   updateState (stateId: string, value: any, extract?: ProjectExtract): Promise<any[]>
//   addStateListener (stateId: string, callback: (state: any) => void, extract?: ProjectExtract): Promise<string[]>
//   removeStateListener (stateId: string, listenerId: string, extract?: ProjectExtract): Promise<any[]>
//   setSyncFrequency (syncTickRate: number, extract?: ProjectExtract): Promise<any[]>
//   setPeerCommunicationCallbacks (
//     discoveryAndConnectionCallback: DiscoConnectionCallback,
//     sendMessageCallback: SendMessageCallback,
//     receiveMessageCallback: ReceiveMessageCallback,
//     extract?: ProjectExtract): Promise<any[]>
// }

// export {
//   DistributedLedgerTechnologyProvider,
//   DiscoConnectionCallback,
//   SendMessageCallback,
//   ReceiveMessageCallback,
//   DistributedLedgerTechnologyMultiplexer
// }
