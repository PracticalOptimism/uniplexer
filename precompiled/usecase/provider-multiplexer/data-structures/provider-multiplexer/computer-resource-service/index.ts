


class ComputerResourceServiceProviderFunctionTable {
  initializeProvider?: any
  createComputerResource?: any
  updateComputerResource?: any
  getComputerResource?: any
  getComputerResourceList?: any
  deleteComputerResource?: any
}


export {
  ComputerResourceServiceProviderFunctionTable
}

