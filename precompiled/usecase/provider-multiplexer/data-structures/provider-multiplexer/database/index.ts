
import { ProjectExtract } from '../../../../../@project/data-structures/extract'
import { ProviderFunctionResponse, InitializeProviderAlgorithm } from '../../provider/provider-function'

declare type CreateItem = (storeId: string, item: any, itemId: string, extract?: ProjectExtract) => CreateItemFunctionResponse
declare type CreateItemAlgorithm = { function: CreateItem }
declare type CreateItemFunctionResponse = Promise<ProviderFunctionResponse<any, DatabaseProviderAdditionalResponseInformationTable>[]>

declare type CreateItemId = (storeId: string, extract?: ProjectExtract) => CreateItemIdFunctionResponse
declare type CreateItemIdAlgorithm = { function: CreateItemId }
declare type CreateItemIdFunctionResponse = Promise<ProviderFunctionResponse<string, DatabaseProviderAdditionalResponseInformationTable>[]>

declare type CreateItemStore = (storeId: string, extract?: ProjectExtract) => CreateItemStoreFunctionResponse
declare type CreateItemStoreAlgorithm = { function: CreateItemStore }
declare type CreateItemStoreFunctionResponse = Promise<ProviderFunctionResponse<any, DatabaseProviderAdditionalResponseInformationTable>[]>

declare type GetItem = (storeId: string, itemId: string, extract?: ProjectExtract) => GetItemFunctionResponse
declare type GetItemAlgorithm = { function: GetItem }
declare type GetItemFunctionResponse = Promise<ProviderFunctionResponse<any, DatabaseProviderAdditionalResponseInformationTable>[]>

declare type GetItemList = (storeId: string, extract?: ProjectExtract) => GetItemListFunctionResponse
declare type GetItemListAlgorithm = { function: GetItemList }
declare type GetItemListFunctionResponse = Promise<ProviderFunctionResponse<any, DatabaseProviderAdditionalResponseInformationTable>[]>

declare type UpdateItem = (storeId: string, itemId: string, item: any, extract?: ProjectExtract) => UpdateItemFunctionResponse
declare type UpdateItemAlgorithm = { function: UpdateItem }
declare type UpdateItemFunctionResponse = Promise<ProviderFunctionResponse<any, DatabaseProviderAdditionalResponseInformationTable>[]>

declare type DeleteItem = (storeId: string, itemId: string, extract?: ProjectExtract) => DeleteItemFunctionResponse
declare type DeleteItemAlgorithm = { function: DeleteItem }
declare type DeleteItemFunctionResponse = Promise<ProviderFunctionResponse<boolean, DatabaseProviderAdditionalResponseInformationTable>[]>

declare type DoesExistItem = (storeId: string, itemId: string, extract?: ProjectExtract) => DoesExistItemFunctionResponse
declare type DoesExistItemAlgorithm = { function: DoesExistItem }
declare type DoesExistItemFunctionResponse = Promise<ProviderFunctionResponse<boolean, DatabaseProviderAdditionalResponseInformationTable>[]>

declare type AddItemEventListener = (storeId: string, itemId: string, eventNameId: string, callbackFunction: any, extract?: ProjectExtract) => AddItemEventListenerFunctionResponse
declare type AddItemEventListenerAlgorithm = { function: AddItemEventListener }
declare type AddItemEventListenerFunctionResponse = Promise<ProviderFunctionResponse<string, DatabaseProviderAdditionalResponseInformationTable>[]>

declare type RemoveItemEventListener = (storeId: string, itemId: string, eventNameId: string, callbackId: string, extract?: ProjectExtract) => RemoveItemEventListenerFunctionResponse
declare type RemoveItemEventListenerAlgorithm = { function: RemoveItemEventListener }
declare type RemoveItemEventListenerFunctionResponse = Promise<ProviderFunctionResponse<any, DatabaseProviderAdditionalResponseInformationTable>[]>

class DatabaseProviderFunctionTable {
  initializeProvider?: InitializeProviderAlgorithm
  createItem?: CreateItemAlgorithm
  createItemId?: CreateItemIdAlgorithm
  createItemStore?: CreateItemStoreAlgorithm
  getItem?: GetItemAlgorithm
  getItemList?: GetItemListAlgorithm
  updateItem?: UpdateItemAlgorithm
  deleteItem?: DeleteItemAlgorithm
  doesExistItem?: DoesExistItemAlgorithm
  addItemEventListener?: AddItemEventListenerAlgorithm
  removeItemEventListener?: RemoveItemEventListenerAlgorithm
}

class DatabaseProviderAdditionalResponseInformationTable {}

export {
  DatabaseProviderFunctionTable,
  DatabaseProviderAdditionalResponseInformationTable,

  CreateItem,
  CreateItemAlgorithm,
  CreateItemFunctionResponse,

  CreateItemId,
  CreateItemIdAlgorithm,
  CreateItemIdFunctionResponse,

  CreateItemStore,
  CreateItemStoreAlgorithm,
  CreateItemStoreFunctionResponse,

  DeleteItem,
  DeleteItemAlgorithm,
  DeleteItemFunctionResponse,

  DoesExistItem,
  DoesExistItemAlgorithm,
  DoesExistItemFunctionResponse,

  GetItem,
  GetItemAlgorithm,
  GetItemFunctionResponse,

  GetItemList,
  GetItemListAlgorithm,
  GetItemListFunctionResponse,

  UpdateItem,
  UpdateItemAlgorithm,
  UpdateItemFunctionResponse,

  AddItemEventListener,
  AddItemEventListenerAlgorithm,
  AddItemEventListenerFunctionResponse,

  RemoveItemEventListener,
  RemoveItemEventListenerAlgorithm,
  RemoveItemEventListenerFunctionResponse
}

