
import * as asymmetricCryptography from './asymmetric-cryptography'
// import * as computerResourceService from './computer-resource-service'
import * as cryptographicHashFunction from './cryptographic-hash-function'
// import * as currencyService from './currency-service'
import * as database from './database'
// import * as distributedLedgerTechnology from './distributed-ledger-technology'
// import * as messageStreamingService from './message-streaming-service'
// import * as networkProtocol from './network-protocol'
// import * as textSearch from './text-search'



class ProviderMultiplexer <ProviderMultiplexerFunctionTable> {
  providerType: string = ''
  providerNameVersionIdTable: { [providerNameVersionId: string]: boolean } = {}
  providerNameVersionIdList: string[] = []
  providerCredentialIdTable: { [providerCredentialId: string]: boolean } = {}

  providerMultiplexerFunctionTable?: ProviderMultiplexerFunctionTable
}

export {
  ProviderMultiplexer,
  asymmetricCryptography,
  // computerResourceService
  cryptographicHashFunction,
  // currencyService,
  database
  // distributedLedgerTechnology,
  // messageStreamingService,
  // networkProtocol,
  // textSearch
}

