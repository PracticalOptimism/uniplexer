
import { ProjectExtract } from '../../../../../@project/data-structures/extract'

import { InitializeProviderAlgorithm, ProviderFunctionResponse } from '../../provider/provider-function'

class AsymmetricCryptographyPrivatePublicKeyPair {
  privateKey: string = ''
  publicKey: string = ''
}

declare type CreatePrivatePublicKeyPair = (extract?: ProjectExtract) => CreatePrivatePublicKeyPairFunctionResponse
declare type CreatePrivatePublicKeyPairAlgorithm = { function: CreatePrivatePublicKeyPair }
declare type CreatePrivatePublicKeyPairFunctionResponse = Promise<ProviderFunctionResponse<AsymmetricCryptographyPrivatePublicKeyPair, AsymmetricCryptographyProviderAdditionalResponseInformationTable>[]>

declare type CreateCryptographicSignature = (messageData: any, privateKey: string, extract?: ProjectExtract) => CreateCryptographicSignatureFunctionResponse
declare type CreateCryptographicSignatureAlgorithm = { function: CreateCryptographicSignature }
declare type CreateCryptographicSignatureFunctionResponse = Promise<ProviderFunctionResponse<any, AsymmetricCryptographyProviderAdditionalResponseInformationTable>[]>

declare type VerifyCryptographicSignature = (messageData: any, cryptographicSignature: any, publicKey: string, extract?: ProjectExtract) => VerifyCryptographicSignatureFunctionResponse
declare type VerifyCryptographicSignatureAlgorithm = { function: VerifyCryptographicSignature }
declare type VerifyCryptographicSignatureFunctionResponse = Promise<ProviderFunctionResponse<boolean, AsymmetricCryptographyProviderAdditionalResponseInformationTable>[]>

class AsymmetricCryptographyProviderFunctionTable {
  initializeProvider?: InitializeProviderAlgorithm
  createPrivatePublicKeyPair?: CreatePrivatePublicKeyPairAlgorithm
  createCryptographicSignature?: CreateCryptographicSignatureAlgorithm
  verifyCryptographicSignature?: VerifyCryptographicSignatureAlgorithm
}

class AsymmetricCryptographyProviderAdditionalResponseInformationTable {
  creationAlgorithm?: string
  verificationAlgorithm?: string
}

export {
  AsymmetricCryptographyPrivatePublicKeyPair,

  CreatePrivatePublicKeyPair,
  CreatePrivatePublicKeyPairAlgorithm,
  CreatePrivatePublicKeyPairFunctionResponse,

  CreateCryptographicSignature,
  CreateCryptographicSignatureAlgorithm,
  CreateCryptographicSignatureFunctionResponse,

  VerifyCryptographicSignature,
  VerifyCryptographicSignatureAlgorithm,
  VerifyCryptographicSignatureFunctionResponse,

  AsymmetricCryptographyProviderFunctionTable,
  AsymmetricCryptographyProviderAdditionalResponseInformationTable
}

