
import { ProjectExtract } from '../../../../../@project/data-structures/extract'
import { ProviderFunctionResponse, InitializeProviderAlgorithm } from '../../provider/provider-function'

declare type CreateSha1CryptographicHashValue = (messageData: any, extract?: ProjectExtract) => CreateSha1CryptographicHashValueFunctionResponse
declare type CreateSha1CryptographicHashValueAlgorithm = { function: CreateSha1CryptographicHashValue }
declare type CreateSha1CryptographicHashValueFunctionResponse = Promise<ProviderFunctionResponse<string, CryptographicHashFunctionProviderAdditionalResponseInformationTable>[]>

declare type CreateSha256CryptographicHashValue = (messageData: any, extract?: ProjectExtract) => CreateSha256CryptographicHashValueFunctionResponse
declare type CreateSha256CryptographicHashValueAlgorithm = { function: CreateSha256CryptographicHashValue }
declare type CreateSha256CryptographicHashValueFunctionResponse = Promise<ProviderFunctionResponse<string, CryptographicHashFunctionProviderAdditionalResponseInformationTable>[]>

class CryptographicHashFunctionProviderFunctionTable {
  initializeProvider?: InitializeProviderAlgorithm
  createSha1CryptographicHashValue?: CreateSha1CryptographicHashValueAlgorithm
  createSha256CryptographicHashValue?: CreateSha256CryptographicHashValueAlgorithm
}

class CryptographicHashFunctionProviderAdditionalResponseInformationTable {}

export {
  CreateSha1CryptographicHashValue,
  CreateSha1CryptographicHashValueAlgorithm,
  CreateSha1CryptographicHashValueFunctionResponse,

  CreateSha256CryptographicHashValue,
  CreateSha256CryptographicHashValueAlgorithm,
  CreateSha256CryptographicHashValueFunctionResponse,

  CryptographicHashFunctionProviderFunctionTable,
  CryptographicHashFunctionProviderAdditionalResponseInformationTable
}

