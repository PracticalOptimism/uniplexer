
class NetworkProtocolProviderFunctionTable {
  initializeProvider?: any

  createMessageTopic?: any
  updateMessageTopic?: any
  getMessageTopic?: any
  getMessageTopicList?: any
  deleteMessageTopic?: any
  getMessageTopicEventListenerList?: any

  createNetworkProtocolAddress?: any
  updateNetworkProtocolAddress?: any
  establishNetworkProtocolConnection?: any
  disconnectFromNetworkProtocolConnection?: any

  dispatchMessage?: any
  addMessageTopicEventListenerForDispatchMessage?: any
  removeMessageTopicEventListenerForDispatchMessage?: any
}

class NetworkProtocolAddress {
  networkProtocolAddressId: string = ''
  networkProtocolName: string = ''
  networkProtocolIndicatorTargetMessage: any
  networkProtocolIndicatorTargetRecipient: any
  networkProtocolIndicatorTargetRespondentAddress: NetworkProtocolAddress = new NetworkProtocolAddress()
  indicatorPresentRequestIndicationMessageAlgorithmTargetName: string = ''
}


export {
  NetworkProtocolProviderFunctionTable,
  NetworkProtocolAddress
}

