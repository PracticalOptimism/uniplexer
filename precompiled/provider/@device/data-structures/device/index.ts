

class CurrentDeviceOperationSettings {
  operatingEnvironmentId: string = ''
  isMobileDevice?: boolean
}

export {
  CurrentDeviceOperationSettings
}

