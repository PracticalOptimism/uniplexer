
import isMobile from 'is-mobile'
import { CurrentDeviceOperationSettings } from '../../data-structures/device'
import { nonObjectReference } from '../../variables/device'
import { OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER, OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS, OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER_WORKER } from '../../../../usecase/provider-multiplexer/constants/provider'

declare var importScripts: any

function initializeCurrentDeviceOperationSettingsFunction () {
  const currentDeviceOperationSettings = new CurrentDeviceOperationSettings()
  currentDeviceOperationSettings.isMobileDevice = isMobile()

  if (typeof (window as any) !== 'undefined') {
    currentDeviceOperationSettings.operatingEnvironmentId = OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER
  } else if (typeof (process as any) !== 'undefined' && process.release.name === 'node') {
    currentDeviceOperationSettings.operatingEnvironmentId = OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS
  } else if (typeof importScripts !== 'undefined') {
    currentDeviceOperationSettings.operatingEnvironmentId = OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER_WORKER
  }

  nonObjectReference.currentDeviceOperationSettings = currentDeviceOperationSettings
}

const initializeCurrentDeviceOperationSettings = {
  function: initializeCurrentDeviceOperationSettingsFunction
}

export {
  initializeCurrentDeviceOperationSettings
}

