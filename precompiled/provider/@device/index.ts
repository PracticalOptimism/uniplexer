

import * as indexAlgorithms from './algorithms'

import * as deviceDataStructures from './data-structures/device'

import * as deviceVariables from './variables/device'

const algorithms = { ...indexAlgorithms }

const constants = {}

const dataStructures = {
  device: { ...deviceDataStructures }
}

const variables = {
  device: { ...deviceVariables }
}

export {
  algorithms,
  constants,
  dataStructures,
  variables
}

