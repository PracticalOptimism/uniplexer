
import { Provider, ProviderWebsiterUrl } from '../../../../usecase/provider-multiplexer/data-structures/provider'
import { PROVIDER_TYPE_DATABASE } from '../../../../usecase/provider-multiplexer/constants/provider-type'

const SERVICE_PROVIDER = new Provider({
  providerNameVersionId: 'couchdb@default',
  providerType: PROVIDER_TYPE_DATABASE,
  providerName: 'CouchDB',
  websiteUrlTable: {
    npmRespositoryWebsiteUrl: new ProviderWebsiterUrl({
      websiteUrl: 'https://www.npmjs.com/package/nano'
    })
  }
})

export {
  SERVICE_PROVIDER
}

