
// const puppeteer = require('puppeteer')

async function createComputerResourceFunction () {
  // const browser = await puppeteer.launch();
  // const page = await browser.newPage();
  // await page.goto('https://example.com');
  // await page.screenshot({path: 'example.png'});

  // await browser.close()
}

const createComputerResource = {
  function: createComputerResourceFunction
}

export {
  createComputerResource
}

/*
Creating a computer resource from Glitch is easy
- go to glitch.com
- click the create remix button
- go to the product page
- and play around with the settings they have available

^^^^
right . . . this is tough to do through an api . . . . 

so we can use a browser playground? like . . 

Doing this from a browser is easy if you're a person with hands and
can navigate on the product page with your keyboard and mouse
and yet, if we want to automatically create a computer resource
from a resource api , i'm having a tough time doing just that
based on the documentational resources i've found on the internet
during my search so far . in other words, i'm finding the documentation
for such an effort to be quite lacking for the features that i 
would like to enjoy .

Feature List:
- create project from program like javascript or http api endpoint
- update project code base running from similarly available consumer interfaces
- update project name
- update project server name?
- what else? not sure . those are a few that pop out to me at this time
- updating the server name to whatever i want is really crucial and important
- allowing the server name to be something like 123456.glitch.me would allow
  the whole internet to have a list of peers to connect to without looking up
  tables of peers from realized server domains . . . well. . . there's still
  a table you need for domains . . but if you can automatically create 
  computer resources at a particular domain . . . . like


*/

