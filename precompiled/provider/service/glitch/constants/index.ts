
import { Provider } from '../../../../usecase/provider-multiplexer/data-structures/provider'
import { GLITCH_DESCRIPTION_OBJECT } from './glitch'

import * as algorithms from '../algorithms'

const SERVICE_PROVIDER: Provider = new Provider(GLITCH_DESCRIPTION_OBJECT)

SERVICE_PROVIDER.functionTable = { ...algorithms }

export {
  SERVICE_PROVIDER
}

