
import { Provider } from '../../../../../usecase/provider-multiplexer/data-structures/provider'
import { PROVIDER_TYPE_COMPUTER_RESOURCE_SERVICE } from '../../../../../usecase/provider-multiplexer/constants/provider-type'


const GLITCH_DESCRIPTION_OBJECT: Provider = new Provider({
  providerNameVersionId: 'glitch@default',
  providerType: PROVIDER_TYPE_COMPUTER_RESOURCE_SERVICE
})

export {
  GLITCH_DESCRIPTION_OBJECT
}

