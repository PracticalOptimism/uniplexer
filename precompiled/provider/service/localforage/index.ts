

import * as indexAlgorithms from './algorithms'

import * as indexConstants from './constants'
import * as localforageConstants from './constants/localforage'

import * as localforageVariables from './variables/localforage'

const algorithms = {
  ...indexAlgorithms
}

const constants = {
  ...indexConstants,
  localforage: { ...localforageConstants }
}

const dataStructures = {
  //
}

const variables = {
  localforage: { ...localforageVariables }
}

export {
  algorithms,
  constants,
  dataStructures,
  variables
}

