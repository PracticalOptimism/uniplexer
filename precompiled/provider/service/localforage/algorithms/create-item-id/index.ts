

import { ProviderFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { CreateItemIdAlgorithm, CreateItemFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/database'

import { LOCALFORAGE_DESCRIPTION_OBJECT } from '../../constants/localforage'

async function createItemIdFunction (_storeId: string): CreateItemFunctionResponse {
  return [new ProviderFunctionResponse({
    providerNameVersionId: LOCALFORAGE_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: Math.random().toString()
  })]
}

const createItemId: CreateItemIdAlgorithm = {
  function: createItemIdFunction
}

export {
  createItemId
}
