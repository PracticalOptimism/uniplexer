
import { ProviderFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { CreateItemAlgorithm, CreateItemFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/database'

import { storeTable } from '../../variables/localforage'
import { createItemStore } from '../create-item-store'
import { createItemId } from '../create-item-id'

import { LOCALFORAGE_DESCRIPTION_OBJECT } from '../../constants/localforage'

async function createItemFunction (storeId: string, item: any, itemId?: string): CreateItemFunctionResponse {
  if (!storeTable[storeId]) {
    await createItemStore.function(storeId)
  }

  itemId = itemId || (await createItemId.function(storeId))[0].providerFunctionResponse || ''

  await storeTable[storeId].setItem(itemId, item)

  return [new ProviderFunctionResponse({
    providerNameVersionId: LOCALFORAGE_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: item
  })]
}

const createItem: CreateItemAlgorithm = {
  function: createItemFunction
}

export {
  createItem
}
