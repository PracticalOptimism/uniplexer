
import { ProviderFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { DoesExistItemAlgorithm, DoesExistItemFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/database'

import { storeTable } from '../../variables/localforage'
import { createItemStore } from '../create-item-store'

import { LOCALFORAGE_DESCRIPTION_OBJECT } from '../../constants/localforage'

async function doesExistItemFunction (storeId: string, itemId: string): DoesExistItemFunctionResponse {
  if (!storeTable[storeId]) {
    await createItemStore.function(storeId)
  }

  const item = await storeTable[storeId].getItem(itemId)

  return [new ProviderFunctionResponse({
    providerNameVersionId: LOCALFORAGE_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: item ? true : false
  })]
}

const doesExistItem: DoesExistItemAlgorithm = {
  function: doesExistItemFunction
}

export {
  doesExistItem
}
