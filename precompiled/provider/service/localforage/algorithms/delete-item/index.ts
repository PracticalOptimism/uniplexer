
import { ProviderFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { DeleteItemAlgorithm, DeleteItemFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/database'

import { storeTable } from '../../variables/localforage'

import { LOCALFORAGE_DESCRIPTION_OBJECT } from '../../constants/localforage'

async function deleteItemFunction (storeId: string, itemId: string): DeleteItemFunctionResponse {
  let isDeletedItem = false

  if (!storeTable[storeId]) {
    return [new ProviderFunctionResponse({
      providerNameVersionId: LOCALFORAGE_DESCRIPTION_OBJECT.providerNameVersionId,
      providerFunctionResponse: isDeletedItem
    })]
  }

  await storeTable[storeId].removeItem(itemId, (err: any) => {
    isDeletedItem = err ? true : false
  })

  return [new ProviderFunctionResponse({
    providerNameVersionId: LOCALFORAGE_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: isDeletedItem
  })]
}

const deleteItem: DeleteItemAlgorithm = {
  function: deleteItemFunction
}

export {
  deleteItem
}
