
import { ProviderFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { GetItemListAlgorithm, GetItemListFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/database'

import { storeTable } from '../../variables/localforage'
import { createItemStore } from '../create-item-store'

import { LOCALFORAGE_DESCRIPTION_OBJECT } from '../../constants/localforage'

async function getItemListFunction (storeId: string): GetItemListFunctionResponse {
  if (!storeTable[storeId]) {
    await createItemStore.function(storeId)
  }

  const resultList: any[] = []

  await storeTable[storeId].iterate((value: any, _: string) => {
    resultList.push(value)
  })

  return [new ProviderFunctionResponse({
    providerNameVersionId: LOCALFORAGE_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: resultList
  })]
}

const getItemList: GetItemListAlgorithm = {
  function: getItemListFunction
}

export {
  getItemList
}
