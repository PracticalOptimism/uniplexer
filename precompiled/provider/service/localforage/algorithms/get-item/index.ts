
import { ProviderFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { GetItemAlgorithm, GetItemFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/database'

import { storeTable } from '../../variables/localforage'
import { createItemStore } from '../create-item-store'

import { LOCALFORAGE_DESCRIPTION_OBJECT } from '../../constants/localforage'

async function getItemFunction (storeId: string, itemId: string): GetItemFunctionResponse {
  if (!storeTable[storeId]) {
    await createItemStore.function(storeId)
  }

  return [new ProviderFunctionResponse({
    providerNameVersionId: LOCALFORAGE_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: await storeTable[storeId].getItem(itemId)
  })]
}

const getItem: GetItemAlgorithm = {
  function: getItemFunction
}

export {
  getItem
}
