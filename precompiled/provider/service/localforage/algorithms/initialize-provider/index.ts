
import { ProjectExtract } from '../../../../../@project/data-structures/extract'
import { InitializeProviderAlgorithm, InitializeProviderFunctionResponse, ProviderFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'

import { rootStore } from '../../variables/localforage'
import { createItemStore } from '../create-item-store'

import { LOCALFORAGE_DESCRIPTION_OBJECT } from '../../constants/localforage'

async function initializeProviderFunction (_configurationObject?: any, _extract?: ProjectExtract): InitializeProviderFunctionResponse {
  try {
    await rootStore.iterate(async (_: any, key: string) => { await createItemStore.function(key) })
  } catch (errorMessage) {
    throw Error(`Cannot initialize provider ${LOCALFORAGE_DESCRIPTION_OBJECT.providerNameVersionId}. Error message: ${errorMessage}`)
  }
  return [new ProviderFunctionResponse({
    providerNameVersionId: LOCALFORAGE_DESCRIPTION_OBJECT.providerNameVersionId
  })]
}

const initializeProvider: InitializeProviderAlgorithm = {
  function: initializeProviderFunction
}

export {
  initializeProvider
}

