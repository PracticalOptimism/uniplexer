
import { ProviderFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { CreateItemStoreAlgorithm, CreateItemFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/database'

import { rootStore, storeTable } from '../../variables/localforage'

import { LOCALFORAGE_DESCRIPTION_OBJECT } from '../../constants/localforage'

async function createItemStoreFunction (storeId: string): CreateItemFunctionResponse {
  await rootStore.setItem(storeId, true)
  storeTable[storeId] = rootStore.createInstance({ name: storeId })

  // this.storeTable[storeId].clear()
  return [new ProviderFunctionResponse({
    providerNameVersionId: LOCALFORAGE_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: storeId
  })]
}

const createItemStore: CreateItemStoreAlgorithm = {
  function: createItemStoreFunction
}

export {
  createItemStore
}
