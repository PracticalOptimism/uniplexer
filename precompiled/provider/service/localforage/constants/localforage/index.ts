

import * as localforage from 'localforage'

import { Provider, ProviderWebsiterUrl, ProviderIconImageUrl, ProviderTextDescription, ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds } from '../../../../../usecase/provider-multiplexer/data-structures/provider'
import { PROVIDER_TYPE_DATABASE } from '../../../../../usecase/provider-multiplexer/constants/provider-type'
import { OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER, OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER_WORKER } from '../../../../../usecase/provider-multiplexer/constants/provider'

import { PROJECT_DESCRIPTION_OBJECT } from '../../../../../@project/constants/project'

const LOCALFORAGE_DESCRIPTION_OBJECT: Provider = new Provider({
  providerNameVersionId: 'localforage@default',
  providerType: PROVIDER_TYPE_DATABASE,
  hasOfflineSupport: true,
  providerName: 'Browser Storage - localForage',
  operatingSystemAndOpenAccessHardwareSpecificationsForPresentComputerOperatingSystem: {
    [OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER]: new ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds({
      presentOperatingEnvironmentId: OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER
    }),
    [OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER_WORKER]: new ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds({
      presentOperatingEnvironmentId: OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER_WORKER
    })
  },
  websiteUrlTable: {
    githubRepositoryWebsiteUrl: new ProviderWebsiterUrl({
      websiteUrl: 'https://github.com/localForage/localForage'
    })
  },
  iconImageUrlTable: {
    defaultIconImageUrl: new ProviderIconImageUrl({
      iconImageUrl: 'http://cdn.onlinewebfonts.com/svg/img_164815.png'
    })
  },
  textDescriptionTable: {
    shortMessageTextDescription: new ProviderTextDescription({
      textDescriptionId: 'shortMessageTextDescriptionForLocalForageDatabaseProvider',
      nameOfAuthorForText: `Jon Ide <https://gitlab.com/practicaloptimism>`,
      nameOfOriginalPublisherForText: JSON.stringify({ publisherName: PROJECT_DESCRIPTION_OBJECT.projectName, publisherNameId: PROJECT_DESCRIPTION_OBJECT.projectId }),
      nameOfSourceForText: `localForage is a fast and simple storage library for JavaScript.
      localForage improves the offline experience of your web app by using asynchronous storage (IndexedDB or WebSQL) with a simple, localStorage-like API.`
    })
  },
  providerImport: localforage
})


const LOCALFORAGE_DATASTORE: string = 'LOCALFORAGE_DATASTORE'

export {
  LOCALFORAGE_DESCRIPTION_OBJECT,
  LOCALFORAGE_DATASTORE
}
