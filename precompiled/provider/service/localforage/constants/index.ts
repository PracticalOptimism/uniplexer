
import { Provider } from '../../../../usecase/provider-multiplexer/data-structures/provider'

import { LOCALFORAGE_DESCRIPTION_OBJECT } from './localforage'

import * as algorithms from '../algorithms'

const SERVICE_PROVIDER: Provider = new Provider(LOCALFORAGE_DESCRIPTION_OBJECT)

SERVICE_PROVIDER.functionTable = { ...algorithms }

export {
  SERVICE_PROVIDER
}

