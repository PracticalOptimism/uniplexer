import * as localforage from 'localforage'

import { LOCALFORAGE_DATASTORE } from '../../constants/localforage'

const rootStore: LocalForage = localforage.createInstance({ name: LOCALFORAGE_DATASTORE })
const storeTable: { [storeName: string]: LocalForage } = {}

export {
  rootStore,
  storeTable
}
