
import { ProjectExtract } from '../../../../../@project/data-structures/extract'
import { CreateCryptographicSignatureAlgorithm, CreateCryptographicSignatureFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/asymmetric-cryptography'
import { ProviderFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'

import { ed25519EllipticCurve } from '../../variables/elliptic'
import { ELLIPTIC_DESCRIPTION_OBJECT, ED25519_CRYPTOGRAPHIC_KEY_CREATION_ALGORITHM } from '../../constants/elliptic'

async function createCryptographicSignatureFunction (messageData: any, privateKey: string, _extract?: ProjectExtract): CreateCryptographicSignatureFunctionResponse {

  // Convert messageData to string buffer
  if (typeof messageData === 'object') {
    messageData = Buffer.from(JSON.stringify(messageData))
  }

  const cryptographicKeyPair = ed25519EllipticCurve.keyFromPrivate(privateKey)
  const cryptographicSignature = ed25519EllipticCurve.sign(messageData, cryptographicKeyPair)

  // Create provider function response
  return [new ProviderFunctionResponse({
    providerNameVersionId: ELLIPTIC_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: cryptographicSignature.toDER(),
    providerAdditionalResponseInformationTable: {
      creationAlgorithm: ED25519_CRYPTOGRAPHIC_KEY_CREATION_ALGORITHM,
      responseType: 'string'
    }
  })]
}

const createCryptographicSignature: CreateCryptographicSignatureAlgorithm = {
  function: createCryptographicSignatureFunction
}

export {
  createCryptographicSignature
}
