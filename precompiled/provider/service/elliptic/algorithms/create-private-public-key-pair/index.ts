
import * as randombytes from 'randombytes'

import { AsymmetricCryptographyPrivatePublicKeyPair, CreatePrivatePublicKeyPairAlgorithm, CreatePrivatePublicKeyPairFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/asymmetric-cryptography'
import { ProviderFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'

import { ed25519EllipticCurve } from '../../variables/elliptic'
import { ELLIPTIC_DESCRIPTION_OBJECT, ED25519_CRYPTOGRAPHIC_KEY_CREATION_ALGORITHM } from '../../constants/elliptic'

async function createPrivatePublicKeyPairFunction (): CreatePrivatePublicKeyPairFunctionResponse {
  const randomBytes = randombytes(16)
  const cryptographicPrivateKey = randomBytes.toString('hex')
  const cryptographicPublicKey = ed25519EllipticCurve.keyFromPrivate(randomBytes).getPublic('hex')

  const privatePublicKeyPair = new AsymmetricCryptographyPrivatePublicKeyPair()
  privatePublicKeyPair.privateKey = cryptographicPrivateKey
  privatePublicKeyPair.publicKey = cryptographicPublicKey

  // Create provider function response
  return [new ProviderFunctionResponse({
    providerNameVersionId: ELLIPTIC_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: privatePublicKeyPair,
    providerAdditionalResponseInformationTable: {
      creationAlgorithm: ED25519_CRYPTOGRAPHIC_KEY_CREATION_ALGORITHM,
      responseType: AsymmetricCryptographyPrivatePublicKeyPair
    }
  })]
}

const createPrivatePublicKeyPair: CreatePrivatePublicKeyPairAlgorithm = {
  function: createPrivatePublicKeyPairFunction
}

export {
  createPrivatePublicKeyPair
}
