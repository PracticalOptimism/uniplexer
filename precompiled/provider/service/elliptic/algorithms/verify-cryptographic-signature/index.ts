
import { ProviderFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { VerifyCryptographicSignatureAlgorithm, VerifyCryptographicSignatureFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/asymmetric-cryptography'
import { ed25519EllipticCurve } from '../../variables/elliptic'

import { ELLIPTIC_DESCRIPTION_OBJECT, ED25519_CRYPTOGRAPHIC_KEY_CREATION_ALGORITHM } from '../../constants/elliptic'

async function verifyCryptographicSignatureFunction (messageData: any, cryptographicSignature: any, publicKey: string): VerifyCryptographicSignatureFunctionResponse {
  const privatePublicKeyPair = ed25519EllipticCurve.keyFromPublic(publicKey, 'hex')

  // Create provider function response
  return [new ProviderFunctionResponse({
    providerNameVersionId: ELLIPTIC_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: ed25519EllipticCurve.verify(messageData, cryptographicSignature, privatePublicKeyPair),
    providerAdditionalResponseInformationTable: {
      verificationAlgorithm: ED25519_CRYPTOGRAPHIC_KEY_CREATION_ALGORITHM,
      responseType: 'boolean'
    }
  })]
}

const verifyCryptographicSignature: VerifyCryptographicSignatureAlgorithm = {
  function: verifyCryptographicSignatureFunction
}

export {
  verifyCryptographicSignature
}
