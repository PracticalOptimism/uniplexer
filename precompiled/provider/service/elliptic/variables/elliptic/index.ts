
import * as elliptic from 'elliptic'

const ed25519EllipticCurve: elliptic.ec = new elliptic.ec('ed25519')

export {
  ed25519EllipticCurve
}
