
import * as elliptic from 'elliptic'

import { Provider, ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds } from '../../../../../usecase/provider-multiplexer/data-structures/provider'
import { PROVIDER_TYPE_ASYMMETRIC_CRYPTOGRAPHY } from '../../../../../usecase/provider-multiplexer/constants/provider-type'
import { OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS, OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS_WORKER, OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER, OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER_WORKER } from '../../../../../usecase/provider-multiplexer/constants/provider'

const ELLIPTIC_DESCRIPTION_OBJECT: Provider = new Provider({
  providerNameVersionId: 'elliptic@default',
  providerType: PROVIDER_TYPE_ASYMMETRIC_CRYPTOGRAPHY,
  hasOfflineSupport: true,
  providerName: 'Elliptic',
  operatingSystemAndOpenAccessHardwareSpecificationsForPresentComputerOperatingSystem: {
    [OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS]: new ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds({
      presentOperatingEnvironmentId: OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS
    }),
    [OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS_WORKER]: new ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds({
      presentOperatingEnvironmentId: OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS_WORKER
    }),
    [OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER]: new ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds({
      presentOperatingEnvironmentId: OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER
    }),
    [OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER_WORKER]: new ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds({
      presentOperatingEnvironmentId: OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER_WORKER
    })
  },
  providerImport: elliptic
})

const ED25519_CRYPTOGRAPHIC_KEY_CREATION_ALGORITHM = 'ed25519'

export {
  ELLIPTIC_DESCRIPTION_OBJECT,
  ED25519_CRYPTOGRAPHIC_KEY_CREATION_ALGORITHM
}
