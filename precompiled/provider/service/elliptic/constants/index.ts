
import { ELLIPTIC_DESCRIPTION_OBJECT } from './elliptic'
import * as algorithms from '../algorithms'

import { Provider } from '../../../../usecase/provider-multiplexer/data-structures/provider'

const SERVICE_PROVIDER = new Provider(ELLIPTIC_DESCRIPTION_OBJECT)

SERVICE_PROVIDER.functionTable = { ...algorithms }

export {
  SERVICE_PROVIDER
}

