

import * as indexAlgorithms from './algorithms'
import * as ellipticVariables from './variables/elliptic'

import * as indexConstants from './constants'
import * as ellipticConstants from './constants/elliptic'

const algorithms = {
  ...indexAlgorithms
}

const dataStructures = {
  //
}

const variables = {
  elliptic: {
    ...ellipticVariables
  }
}

const constants = {
  ...indexConstants,
  elliptic: {
    ...ellipticConstants
  }
}

export {
  algorithms,
  dataStructures,
  variables,
  constants
}
