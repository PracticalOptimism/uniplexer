
import { Provider } from '../../../../usecase/provider-multiplexer/data-structures/provider'
import { PROVIDER_TYPE_NETWORK_PROTOCOL } from '../../../../usecase/provider-multiplexer/constants/provider-type'

const SERVICE_PROVIDER: Provider = new Provider({
  providerNameVersionId: 'webrtc@default',
  providerType: PROVIDER_TYPE_NETWORK_PROTOCOL
})

export {
  SERVICE_PROVIDER
}
