
import * as crypto from 'crypto-js'

import { ProviderFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { CreateSha1CryptographicHashValueAlgorithm, CreateSha1CryptographicHashValueFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/cryptographic-hash-function'

import { CRYPTO_JS_DESCRIPTION_OBJECT } from '../../constants/crypto-js'

async function createSha1CryptographicHashValueFunction (messageData: any): CreateSha1CryptographicHashValueFunctionResponse {
  return [new ProviderFunctionResponse({
    providerNameVersionId: CRYPTO_JS_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: crypto.SHA1(messageData).toString()
  })]
}

const createSha1CryptographicHashValue: CreateSha1CryptographicHashValueAlgorithm = {
  function: createSha1CryptographicHashValueFunction
}

export {
  createSha1CryptographicHashValue
}

