
import * as crypto from 'crypto-js'

import { ProviderFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { CreateSha256CryptographicHashValueAlgorithm, CreateSha256CryptographicHashValueFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/cryptographic-hash-function'

import { CRYPTO_JS_DESCRIPTION_OBJECT } from '../../constants/crypto-js'

async function createSha256CryptographicHashValueFunction (messageData: any): CreateSha256CryptographicHashValueFunctionResponse {
  return [new ProviderFunctionResponse({
    providerNameVersionId: CRYPTO_JS_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: crypto.SHA256(messageData).toString()
  })]
}

const createSha256CryptographicHashValue: CreateSha256CryptographicHashValueAlgorithm = {
  function: createSha256CryptographicHashValueFunction
}

export {
  createSha256CryptographicHashValue
}
