
import * as crypto from 'crypto-js'

import { Provider, ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds } from '../../../../../usecase/provider-multiplexer/data-structures/provider'
import { PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION } from '../../../../../usecase/provider-multiplexer/constants/provider-type'
import { OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS, OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS_WORKER, OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER, OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER_WORKER } from '../../../../../usecase/provider-multiplexer/constants/provider'

const CRYPTO_JS_DESCRIPTION_OBJECT: Provider = new Provider({
  providerNameVersionId: 'crypto-js@default',
  providerType: PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION,
  hasOfflineSupport: true,
  providerName: 'crypto-js',
  operatingSystemAndOpenAccessHardwareSpecificationsForPresentComputerOperatingSystem: {
    [OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS]: new ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds({
      presentOperatingEnvironmentId: OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS
    }),
    [OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS_WORKER]: new ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds({
      presentOperatingEnvironmentId: OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS_WORKER
    }),
    [OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER]: new ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds({
      presentOperatingEnvironmentId: OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER
    }),
    [OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER_WORKER]: new ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds({
      presentOperatingEnvironmentId: OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER_WORKER
    })
  },
  providerImport: crypto
})

export {
  CRYPTO_JS_DESCRIPTION_OBJECT
}
