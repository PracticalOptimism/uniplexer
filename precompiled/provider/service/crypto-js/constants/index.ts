

import { Provider } from '../../../../usecase/provider-multiplexer/data-structures/provider'
import { CRYPTO_JS_DESCRIPTION_OBJECT } from './crypto-js'

import * as algorithms from '../algorithms'

const SERVICE_PROVIDER = new Provider(CRYPTO_JS_DESCRIPTION_OBJECT)

SERVICE_PROVIDER.functionTable = { ...algorithms }

export {
  SERVICE_PROVIDER
}

