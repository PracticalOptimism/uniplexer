
import * as BrowserFS from 'browserfs'


const windowObj = ((typeof window === 'undefined') ? {} : window) as Window

BrowserFS.install(windowObj)

// Configures BrowserFS to use the LocalStorage file system.
// browserfs.configure({
//   fs: 'LocalStorage'
// }, (err: any) => {
//   if (err) {
//     throw err
//   }
// })

