
import * as objectHash from 'object-hash'

import { ProviderFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { CreateSha1CryptographicHashValueAlgorithm, CreateSha1CryptographicHashValueFunctionResponse } from '../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/cryptographic-hash-function'
import { OBJECT_HASH_DESCRIPTION_OBJECT } from '../../constants/object-hash'

async function createSha1CryptographicHashValueFunction (messageData: any): CreateSha1CryptographicHashValueFunctionResponse {
  return [new ProviderFunctionResponse({
    providerNameVersionId: OBJECT_HASH_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: objectHash.sha1(messageData)
  })]
}

const createSha1CryptographicHashValue: CreateSha1CryptographicHashValueAlgorithm = {
  function: createSha1CryptographicHashValueFunction
}

export {
  createSha1CryptographicHashValue
}

