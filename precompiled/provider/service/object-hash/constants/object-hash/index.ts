
import * as objectHash from 'object-hash'

import { Provider, ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds } from '../../../../../usecase/provider-multiplexer/data-structures/provider'
import { PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION } from '../../../../../usecase/provider-multiplexer/constants/provider-type'
import { OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS, OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS_WORKER, OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER, OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER_WORKER } from '../../../../../usecase/provider-multiplexer/constants/provider'

const OBJECT_HASH_DESCRIPTION_OBJECT: Provider = new Provider({
  providerNameVersionId: 'object-hash@default',
  providerType: PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION,
  hasOfflineSupport: true,
  providerName: 'object-hash',
  operatingSystemAndOpenAccessHardwareSpecificationsForPresentComputerOperatingSystem: {
    [OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS]: new ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds({
      presentOperatingEnvironmentId: OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS
    }),
    [OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS_WORKER]: new ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds({
      presentOperatingEnvironmentId: OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_NODE_JS_WORKER
    }),
    [OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER]: new ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds({
      presentOperatingEnvironmentId: OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER
    }),
    [OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER_WORKER]: new ProviderOperatingSystemRequestRequirementForPresentOperatingSystemEveryNerdNeeds({
      presentOperatingEnvironmentId: OPERATING_ENVIRONMENT_JAVASCRIPT_RUNTIME_WEB_BROWSER_WORKER
    })
  },
  providerImport: objectHash
})

export {
  OBJECT_HASH_DESCRIPTION_OBJECT
}
