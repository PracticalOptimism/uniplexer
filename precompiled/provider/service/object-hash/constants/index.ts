

import { Provider } from '../../../../usecase/provider-multiplexer/data-structures/provider'
import { OBJECT_HASH_DESCRIPTION_OBJECT } from './object-hash'

import * as algorithms from '../algorithms'

const SERVICE_PROVIDER = new Provider(OBJECT_HASH_DESCRIPTION_OBJECT)

SERVICE_PROVIDER.functionTable = { ...algorithms }

export {
  SERVICE_PROVIDER
}
