

import * as indexAlgorithms from './algorithms'
import * as indexConstants from './constants'

const algorithms = {
  ...indexAlgorithms
}

const dataStructures = {
  //
}

const variables = {
  //
}

const constants = {
  ...indexConstants
}

export {
  algorithms,
  dataStructures,
  variables,
  constants
}
