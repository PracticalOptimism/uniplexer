
import { Provider } from '../../../../usecase/provider-multiplexer/data-structures/provider'
import { PROVIDER_TYPE_DATABASE } from '../../../../usecase/provider-multiplexer/constants/provider-type'

const SERVICE_PROVIDER: Provider = new Provider({
  providerNameVersionId: 'webtorrent@default',
  providerType: PROVIDER_TYPE_DATABASE
})

export {
  SERVICE_PROVIDER
}
