import { Provider } from '../../../../usecase/provider-multiplexer/data-structures/provider'
import { PROVIDER_TYPE_COMPUTER_RESOURCE_SERVICE } from '../../../../usecase/provider-multiplexer/constants/provider-type'


const SERVICE_PROVIDER: Provider = new Provider({
  providerNameVersionId: 'azure@default',
  providerType: PROVIDER_TYPE_COMPUTER_RESOURCE_SERVICE
})

export {
  SERVICE_PROVIDER
}

