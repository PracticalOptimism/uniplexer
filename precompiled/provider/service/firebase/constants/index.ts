import { Provider } from '../../../../usecase/provider-multiplexer/data-structures/provider'
import { PROVIDER_TYPE_DATABASE } from '../../../../usecase/provider-multiplexer/constants/provider-type'

const FIREBASE_REALTIME_DATABASE: Provider = new Provider({
  providerNameVersionId: 'firebase/realtime-database@default',
  providerType: PROVIDER_TYPE_DATABASE
})

const FIREBASE_FIRESTORE: Provider = new Provider({
  providerNameVersionId: 'firebase/firestore@default',
  providerType: PROVIDER_TYPE_DATABASE
})

const FIREBASE_STATIC_HOSTING: Provider = new Provider({
  providerNameVersionId: 'firebase/static-hosting@default',
  providerType: PROVIDER_TYPE_DATABASE
})

export {
  FIREBASE_REALTIME_DATABASE,
  FIREBASE_FIRESTORE,
  FIREBASE_STATIC_HOSTING
}

