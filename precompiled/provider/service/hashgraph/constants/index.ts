
import { Provider } from '../../../../usecase/provider-multiplexer/data-structures/provider'
import { PROVIDER_TYPE_DISTRIBUTED_LEDGER_TECHNOLOGY } from '../../../../usecase/provider-multiplexer/constants/provider-type'

const SERVICE_PROVIDER: Provider = new Provider({
  providerNameVersionId: '@practicaloptimism/hashgraph@default',
  providerType: PROVIDER_TYPE_DISTRIBUTED_LEDGER_TECHNOLOGY
})

export {
  SERVICE_PROVIDER
}

