

class HashgraphLedger {
  ledgerId: string = ''

  latestStateVersionHashId: string = ''
  stateVersionTable: { [stateVersionHashId: string]: any } = {}

  peerListenerTable: any = {}

  syncFrequency: number = 1000
  peerDiscoverCallbackMethod: any
  peerSendMessageCallbackMethod: any
  peerReceiveMessageCallbackMethod: any
}

export {
  HashgraphLedger
}