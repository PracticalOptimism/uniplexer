
import { ProjectExtract } from '../../../../../../@project/data-structures/extract'
import { ProviderFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { CreateItemStoreAlgorithm, CreateItemFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/database'

import { storeTable } from '../../variables/database'

import { MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT } from '../../constants/database'

async function createItemStoreFunction (storeId: string, _extract?: ProjectExtract): CreateItemFunctionResponse {
  storeTable[storeId] = {}

  return [new ProviderFunctionResponse({
    providerNameVersionId: MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: storeId
  })]
}

const createItemStore: CreateItemStoreAlgorithm = {
  function: createItemStoreFunction
}

export {
  createItemStore
}
