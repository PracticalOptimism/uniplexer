
import { ProjectExtract } from '../../../../../../@project/data-structures/extract'
import { ProviderFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { UpdateItemAlgorithm, UpdateItemFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/database'

import { createItemStore } from '../create-item-store'
import { storeTable } from '../../variables/database'

import { MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT } from '../../constants/database'

async function updateItemFunction (storeId: string, itemId: string, item: any, _extract?: ProjectExtract): UpdateItemFunctionResponse {
  if (!storeTable[storeId]) {
    await createItemStore.function(storeId)
  }

  storeTable[storeId][itemId] = item

  return [new ProviderFunctionResponse({
    providerNameVersionId: MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: item
  })]
}

const updateItem: UpdateItemAlgorithm = {
  function: updateItemFunction
}

export {
  updateItem
}

