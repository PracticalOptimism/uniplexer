
import { ProjectExtract } from '../../../../../../@project/data-structures/extract'
import { ProviderFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { DoesExistItemAlgorithm, DoesExistItemFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/database'

import { createItemStore } from '../create-item-store'
import { storeTable } from '../../variables/database'

import { MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT } from '../../constants/database'

async function doesExistItemFunction (storeId: string, itemId: string, _extract?: ProjectExtract): DoesExistItemFunctionResponse {
  if (!storeTable[storeId]) {
    await createItemStore.function(storeId)
  }

  const doesExist = storeTable[storeId][itemId]

  return [new ProviderFunctionResponse({
    providerNameVersionId: MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: doesExist
  })]
}

const doesExistItem: DoesExistItemAlgorithm = {
  function: doesExistItemFunction
}

export {
  doesExistItem
}

