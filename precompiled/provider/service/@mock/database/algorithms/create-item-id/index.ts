
import { ProjectExtract } from '../../../../../../@project/data-structures/extract'
import { ProviderFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { CreateItemIdAlgorithm, CreateItemFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/database'

import { MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT } from '../../constants/database'

async function createItemIdFunction (_storeId: string, _extract?: ProjectExtract): CreateItemFunctionResponse {
  return [new ProviderFunctionResponse({
    providerNameVersionId: MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: Math.random().toString()
  })]
}

const createItemId: CreateItemIdAlgorithm = {
  function: createItemIdFunction
}

export {
  createItemId
}

