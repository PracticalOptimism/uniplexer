
import { ProjectExtract } from '../../../../../../@project/data-structures/extract'
import { ProviderFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { CreateItemAlgorithm, CreateItemFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/database'

import { createItemStore } from '../create-item-store'
import { storeTable } from '../../variables/database'

import { MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT } from '../../constants/database'

async function createItemFunction (storeId: string, item: any, itemId?: string, _extract?: ProjectExtract): CreateItemFunctionResponse {
  if (!storeTable[storeId]) {
    await createItemStore.function(storeId)
  }

  if (!itemId) {
    itemId = Math.random().toString()
  }

  storeTable[storeId][itemId] = item

  return [new ProviderFunctionResponse({
    providerNameVersionId: MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: item
  })]
}

const createItem: CreateItemAlgorithm = {
  function: createItemFunction
}

export {
  createItem
}

