
import { ProjectExtract } from '../../../../../../@project/data-structures/extract'
import { ProviderFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { DeleteItemAlgorithm, DeleteItemFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/database'

import { storeTable } from '../../variables/database'

import { MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT } from '../../constants/database'

async function deleteItemFunction (storeId: string, itemId: string, _extract?: ProjectExtract): DeleteItemFunctionResponse {
  if (!storeTable[storeId]) {
    return [new ProviderFunctionResponse({
      providerNameVersionId: MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT.providerNameVersionId,
      providerFunctionResponse: false
    })]
  }

  delete storeTable[storeId][itemId]

  return [new ProviderFunctionResponse({
    providerNameVersionId: MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: true
  })]
}

const deleteItem: DeleteItemAlgorithm = {
  function: deleteItemFunction
}

export {
  deleteItem
}

