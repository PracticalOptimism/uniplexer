
import { ProjectExtract } from '../../../../../../@project/data-structures/extract'
import { ProviderFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { GetItemListAlgorithm, GetItemListFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/database'

import { createItemStore } from '../create-item-store'
import { storeTable } from '../../variables/database'

import { MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT } from '../../constants/database'

async function getItemListFunction (storeId: string, _extract?: ProjectExtract): GetItemListFunctionResponse {
  if (!storeTable[storeId]) {
    await createItemStore.function(storeId)
  }

  const resultList: any[] = Object.keys(storeTable[storeId]).map((itemId) => storeTable[itemId])

  return [new ProviderFunctionResponse({
    providerNameVersionId: MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: resultList
  })]
}

const getItemList: GetItemListAlgorithm = {
  function: getItemListFunction
}

export {
  getItemList
}

