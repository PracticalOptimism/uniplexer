
import { ProjectExtract } from '../../../../../../@project/data-structures/extract'
import { ProviderFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { GetItemAlgorithm, GetItemFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/database'

import { createItemStore } from '../create-item-store'
import { storeTable } from '../../variables/database'

import { MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT } from '../../constants/database'

async function getItemFunction (storeId: string, itemId: string, _extract?: ProjectExtract): GetItemFunctionResponse {
  if (!storeTable[storeId]) {
    await createItemStore.function(storeId)
  }

  return [new ProviderFunctionResponse({
    providerNameVersionId: MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: storeTable[storeId][itemId]
  })]
}

const getItem: GetItemAlgorithm = {
  function: getItemFunction
}

export {
  getItem
}

