

import { PROVIDER_TYPE_DATABASE } from '../../../../../../usecase/provider-multiplexer/constants/provider-type'

import { Provider } from '../../../../../../usecase/provider-multiplexer/data-structures/provider'


const MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT = new Provider({
  providerNameVersionId: 'mock-database@default',
  providerType: PROVIDER_TYPE_DATABASE,
  hasOfflineSupport: true,
  isMockProvider: true
})

export {
  MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT
}

