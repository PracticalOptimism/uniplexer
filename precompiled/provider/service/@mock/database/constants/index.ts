
import { Provider } from '../../../../../usecase/provider-multiplexer/data-structures/provider'

import * as algorithms from '../algorithms'

import { MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT } from './database'

const SERVICE_PROVIDER = new Provider(MOCK_PROVIDER_DATABASE_DESCRIPTION_OBJECT)

SERVICE_PROVIDER.functionTable = { ...algorithms }

export {
  SERVICE_PROVIDER
}

