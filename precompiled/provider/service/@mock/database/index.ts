

import * as indexAlgorithms from './algorithms'

import * as indexConstants from './constants'
import * as databaseConstants from './constants/database'

import * as databaseVariables from './variables/database'

const algorithms = {
  ...indexAlgorithms
}

const constants = {
  ...indexConstants,
  database: { ...databaseConstants }
}

const dataStructures = {
  //
}

const variables = {
  database: {
    ...databaseVariables
  }
}

export {
  algorithms,
  constants,
  dataStructures,
  variables
}
