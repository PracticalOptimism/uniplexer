
import { Provider } from '../../../../../../usecase/provider-multiplexer/data-structures/provider'
import { PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION } from '../../../../../../usecase/provider-multiplexer/constants/provider-type'


const MOCK_PROVIDER_CRYPTOGRAPHIC_HASH_FUNCTION_DESCRIPTION_OBJECT = new Provider({
  providerNameVersionId: 'mock-hash-function@default',
  providerType: PROVIDER_TYPE_CRYPTOGRAPHIC_HASH_FUNCTION,
  hasOfflineSupport: true,
  isMockProvider: true
})

export {
  MOCK_PROVIDER_CRYPTOGRAPHIC_HASH_FUNCTION_DESCRIPTION_OBJECT
}

