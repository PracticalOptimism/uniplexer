
import { ProjectExtract } from '../../../../../../@project/data-structures/extract'
import { InitializeProviderAlgorithm, InitializeProviderFunctionResponse, ProviderFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'

async function initializeProviderFunction (_configurationObject?: any, _extract?: ProjectExtract): InitializeProviderFunctionResponse {
  return [new ProviderFunctionResponse()]
}

const initializeProvider: InitializeProviderAlgorithm = {
  function: initializeProviderFunction
}

export {
  initializeProvider
}

