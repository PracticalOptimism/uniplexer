
import { ProjectExtract } from '../../../../../../@project/data-structures/extract'
import { ProviderFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { CreateSha1CryptographicHashValueAlgorithm, CreateSha1CryptographicHashValueFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/cryptographic-hash-function'

import { MOCK_PROVIDER_CRYPTOGRAPHIC_HASH_FUNCTION_DESCRIPTION_OBJECT } from '../../constants/cryptographic-hash-function'

async function createSha1CryptographicHashValueFunction (_messageData: any, _extract?: ProjectExtract): CreateSha1CryptographicHashValueFunctionResponse {
  return [new ProviderFunctionResponse({
    providerNameVersionId: MOCK_PROVIDER_CRYPTOGRAPHIC_HASH_FUNCTION_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: Math.random().toString()
  })]
}

const createSha1CryptographicHashValue: CreateSha1CryptographicHashValueAlgorithm = {
  function: createSha1CryptographicHashValueFunction
}

export {
  createSha1CryptographicHashValue
}

