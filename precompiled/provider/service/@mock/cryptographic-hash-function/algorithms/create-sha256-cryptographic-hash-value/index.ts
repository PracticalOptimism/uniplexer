
import { ProjectExtract } from '../../../../../../@project/data-structures/extract'
import { ProviderFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { CreateSha256CryptographicHashValueAlgorithm, CreateSha256CryptographicHashValueFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/cryptographic-hash-function'

import { MOCK_PROVIDER_CRYPTOGRAPHIC_HASH_FUNCTION_DESCRIPTION_OBJECT } from '../../constants/cryptographic-hash-function'

async function createSha256CryptographicHashValueFunction (_messageData: any, _extract?: ProjectExtract): CreateSha256CryptographicHashValueFunctionResponse {
  return [new ProviderFunctionResponse({
    providerNameVersionId: MOCK_PROVIDER_CRYPTOGRAPHIC_HASH_FUNCTION_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: Math.random().toString()
  })]
}

const createSha256CryptographicHashValue: CreateSha256CryptographicHashValueAlgorithm = {
  function: createSha256CryptographicHashValueFunction
}

export {
  createSha256CryptographicHashValue
}
