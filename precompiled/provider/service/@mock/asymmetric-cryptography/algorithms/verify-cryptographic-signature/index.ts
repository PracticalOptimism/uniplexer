
import { ProjectExtract } from '../../../../../../@project/data-structures/extract'
import { ProviderFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { VerifyCryptographicSignatureAlgorithm, VerifyCryptographicSignatureFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/asymmetric-cryptography'

import { MOCK_PROVIDER_ASYMMETRIC_CRYPTOGRAPHY_DESCRIPTION_OBJECT } from '../../constants/asymmetric-cryptography'

async function verifyCryptographicSignatureFunction (_messageData: any, _cryptographicSignature: any, _publicKey: string, _extract?: ProjectExtract): VerifyCryptographicSignatureFunctionResponse {
  return [new ProviderFunctionResponse({
    providerNameVersionId: MOCK_PROVIDER_ASYMMETRIC_CRYPTOGRAPHY_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: true
  })]
}

const verifyCryptographicSignature: VerifyCryptographicSignatureAlgorithm = {
  function: verifyCryptographicSignatureFunction
}

export {
  verifyCryptographicSignature
}

