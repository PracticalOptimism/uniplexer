
import { ProjectExtract } from '../../../../../../@project/data-structures/extract'
import { ProviderFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { CreateCryptographicSignatureAlgorithm, CreateCryptographicSignatureFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/asymmetric-cryptography'

import { MOCK_PROVIDER_ASYMMETRIC_CRYPTOGRAPHY_DESCRIPTION_OBJECT } from '../../constants/asymmetric-cryptography'

async function createCryptographicSignatureFunction (_messageData: any, _privateKey: string, _extract?: ProjectExtract): CreateCryptographicSignatureFunctionResponse {
  return [new ProviderFunctionResponse({
    providerNameVersionId: MOCK_PROVIDER_ASYMMETRIC_CRYPTOGRAPHY_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: Math.random().toString()
  })]
}

const createCryptographicSignature: CreateCryptographicSignatureAlgorithm = {
  function: createCryptographicSignatureFunction
}

export {
  createCryptographicSignature
}
