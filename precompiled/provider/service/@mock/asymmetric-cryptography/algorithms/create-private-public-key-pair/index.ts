
import { ProjectExtract } from '../../../../../../@project/data-structures/extract'
import { ProviderFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider/provider-function'
import { AsymmetricCryptographyPrivatePublicKeyPair, CreatePrivatePublicKeyPairAlgorithm, CreatePrivatePublicKeyPairFunctionResponse } from '../../../../../../usecase/provider-multiplexer/data-structures/provider-multiplexer/asymmetric-cryptography'

import { MOCK_PROVIDER_ASYMMETRIC_CRYPTOGRAPHY_DESCRIPTION_OBJECT } from '../../constants/asymmetric-cryptography'

async function createPrivatePublicKeyPairFunction (_extract?: ProjectExtract): CreatePrivatePublicKeyPairFunctionResponse {
  const keyPair = new AsymmetricCryptographyPrivatePublicKeyPair()
  keyPair.privateKey = Math.random().toString()
  keyPair.publicKey = Math.random().toString()
  return [new ProviderFunctionResponse({
    providerNameVersionId: MOCK_PROVIDER_ASYMMETRIC_CRYPTOGRAPHY_DESCRIPTION_OBJECT.providerNameVersionId,
    providerFunctionResponse: keyPair
  })]
}

const createPrivatePublicKeyPair: CreatePrivatePublicKeyPairAlgorithm = {
  function: createPrivatePublicKeyPairFunction
}

export {
  createPrivatePublicKeyPair
}
