
import { Provider } from '../../../../../../usecase/provider-multiplexer/data-structures/provider'
import { PROVIDER_TYPE_ASYMMETRIC_CRYPTOGRAPHY } from '../../../../../../usecase/provider-multiplexer/constants/provider-type'

const MOCK_PROVIDER_ASYMMETRIC_CRYPTOGRAPHY_DESCRIPTION_OBJECT = new Provider({
  providerNameVersionId: 'mock-asymmetric-cryptography@default',
  providerType: PROVIDER_TYPE_ASYMMETRIC_CRYPTOGRAPHY,
  hasOfflineSupport: true,
  isMockProvider: true
})

const CRYPTOGRAPHY_KEY_ALGORITHM = 'Math.random()'

export {
  CRYPTOGRAPHY_KEY_ALGORITHM,
  MOCK_PROVIDER_ASYMMETRIC_CRYPTOGRAPHY_DESCRIPTION_OBJECT
}
