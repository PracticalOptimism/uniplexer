
// const client = solr.createClient()
const client = ({} as any)

// Switch on "auto commit", by default `client.autoCommit = false`
client.autoCommit = true

export {
  client
}