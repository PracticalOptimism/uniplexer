import { Provider, ProviderWebsiterUrl } from '../../../../usecase/provider-multiplexer/data-structures/provider'
import { PROVIDER_TYPE_TEXT_SEARCH } from '../../../../usecase/provider-multiplexer/constants/provider-type'

const SERVICE_PROVIDER: Provider = new Provider({
  providerNameVersionId: 'solr@default',
  providerType: PROVIDER_TYPE_TEXT_SEARCH,
  websiteUrlTable: {
    npmRespositoryWebsiteUrl: new ProviderWebsiterUrl({
      websiteUrl: 'https://www.npmjs.com/package/solr-client'
    })
  }
})

export {
  SERVICE_PROVIDER
}
