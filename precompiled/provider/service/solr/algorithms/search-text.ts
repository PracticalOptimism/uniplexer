
import { client } from '../variables'

// http://lbdremy.github.io/solr-node-client/code/search.js.html
async function searchText (text: string, propertyList?: string[], rangeMin?: number, numOfRows?: number): Promise<any[]> {

  if (!rangeMin) { rangeMin = 0 }
  if (!numOfRows) { numOfRows = 100 }

  if (!propertyList) { return Promise.resolve([])}

  const query: { [key: string]: string } = {}
  for (let i = 0; i < propertyList.length; i++) {
    query[propertyList[i]] = text
  }

  // Lucene query
  const luceneQuery = client.createQuery().q(query).start(rangeMin).rows(numOfRows)

  return new Promise((resolve, reject) => {
    client.search(luceneQuery, (err: any, obj: any) => {
      if (err) {
        reject(`Solr search error: ${err}`)
      } else {
        resolve(obj)
      }
    })
  })
}

export {
  searchText
}