
import { client } from '../variables'

// http://lbdremy.github.io/solr-node-client/code/add.js.html
// http://lbdremy.github.io/solr-node-client/code/addRemoteResource.js.html
async function indexText (itemList: any[]): Promise<any> {
  // Add documents
  return new Promise((resolve, reject) => {
      client.add(itemList, (err: any, obj: any) => {
      if (err) {
        reject(`Solr index error: ${err}`)
      } else {
        resolve(obj)
      }
    })
  })
}

export {
  indexText
}