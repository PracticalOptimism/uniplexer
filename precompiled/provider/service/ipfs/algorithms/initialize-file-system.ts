

import * as ipfs from 'ipfs'

import * as orbitdb from 'orbit-db'

export {
  ipfs,
  orbitdb
}