import { Provider, ProviderWebsiterUrl } from '../../../../usecase/provider-multiplexer/data-structures/provider'
import { PROVIDER_TYPE_TEXT_SEARCH } from '../../../../usecase/provider-multiplexer/constants/provider-type'

const SERVICE_PROVIDER: Provider = new Provider({
  providerNameVersionId: 'lunr@default',
  providerType: PROVIDER_TYPE_TEXT_SEARCH,
  websiteUrlTable: {
    npmRepositoryWebsiteUrl: new ProviderWebsiterUrl({
      websiteUrl: 'https://www.npmjs.com/package/lunr'
    })
  }
})

export {
  SERVICE_PROVIDER
}
