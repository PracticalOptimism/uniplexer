
import { indexList } from '../variables'

async function searchText (text: string, _propertyList?: string[], rangeMin?: number, numOfRows?: number): Promise<any[]> {

  if (!rangeMin) { rangeMin = 0 }
  if (!numOfRows) { numOfRows = 100 }

  const resultList: lunr.Index.Result[] = []

  for (let i = 0; i < indexList.length; i++) {
    const tempResultList = indexList[i].search(text)
    resultList.push(...tempResultList)

  }

  return resultList
}


export {
  searchText
}