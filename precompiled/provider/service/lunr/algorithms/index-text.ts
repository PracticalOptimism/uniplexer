
import * as lunr from 'lunr'

import { indexList } from '../variables'

async function indexText (itemList: any[]): Promise<any> {
  const lunrIndex = lunr(function (this: any) {
    for (let i = 0; i < itemList.length; i++) {
      for (const property of itemList[i]) {
        (this as any).field(property)
      }
      (this as any).add(itemList[i])
    }
  })

  indexList.push(lunrIndex)
}

export {
  indexText
}