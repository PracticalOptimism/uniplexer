
import { Provider, ProviderWebsiterUrl } from '../../../../usecase/provider-multiplexer/data-structures/provider'
import { PROVIDER_TYPE_COMPUTER_RESOURCE_SERVICE } from '../../../../usecase/provider-multiplexer/constants/provider-type'

const SERVICE_PROVIDER: Provider = new Provider({
  providerNameVersionId: 'zeit@default',
  providerType: PROVIDER_TYPE_COMPUTER_RESOURCE_SERVICE,
  websiteUrlTable: {
    homeWebsiteUrl: new ProviderWebsiterUrl({
      websiteUrl: 'https://zeit.co/docs/api#getting-started'
    })
  }
})

export {
  SERVICE_PROVIDER
}

