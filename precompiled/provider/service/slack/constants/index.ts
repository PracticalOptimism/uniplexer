
import { Provider } from '../../../../usecase/provider-multiplexer/data-structures/provider'
import { PROVIDER_TYPE_MESSAGE_STREAMING_SERVICE } from '../../../../usecase/provider-multiplexer/constants/provider-type'

const SERVICE_PROVIDER: Provider = new Provider({
  providerNameVersionId: 'slack@default',
  providerType: PROVIDER_TYPE_MESSAGE_STREAMING_SERVICE
})

export {
  SERVICE_PROVIDER
}
