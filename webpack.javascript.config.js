
const path = require('path')

module.exports = {
  target: 'web',
  mode: 'development',
  entry: {
    index: './precompiled/javascript.ts',
  },
  output: {
    filename: '[name].js',
    libraryTarget: 'umd',
    path: path.resolve(__dirname, 'compiled')
  },
  module: {
    rules: [{
      test: /\.tsx?$/,
      use: 'ts-loader',
      exclude: /node_modules/
    }]
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
    alias: {
      'precompiled': path.resolve(__dirname, './precompiled'),
      '@': path.resolve(__dirname, './precompiled')
    }
  },
  devtool: false
}

