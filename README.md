# 🦄 Uniplexer

### A multiplexer for 3rd party services and application protocols

##### Reference Links: [Demos](https://practicaloptimism.gitlab.io/uniplexer) | [Video Introduction](https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g) | [Video Tutorial](https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g) | [Live Programming Development Journal](https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g)

##### This Documentation Page was Last Updated on May 22, A.D. 2020

##### Project Development Status Updates
[![npm version](https://badge.fury.io/js/%40practicaloptimism%2Funiplexer.svg)](https://badge.fury.io/js/%40practicaloptimism%2Funiplexer) [![downloads](https://img.shields.io/npm/dt/@practicaloptimism/uniplexer)](https://www.npmjs.com/package/%40practicaloptimism/uniplexer) [![coverage report](https://gitlab.com/practicaloptimism/uniplexer/badges/master/coverage.svg)](https://gitlab.com/practicaloptimism/uniplexer/-/commits/master) [![pipeline status](https://gitlab.com/practicaloptimism/uniplexer/badges/master/pipeline.svg)](https://gitlab.com/practicaloptimism/uniplexer/-/commits/master)

### Installation

##### Node Package Manager (NPM) Installation
```
npm install --save @practicaloptimism/uniplexer
```

##### Script import from JavaScript (requires NPM installation)
```javascript
import * as uniplexer from '@practicaloptimism/uniplexer'

```

##### HTML Script Import
```html
<script src="https://unpkg.com/@practicaloptimism/uniplexer"></script>
```

### Getting Started: Example Usecase

```javascript
// Get the database function table
const { database } = uniplexer.usecase.providerMultiplexer.algorithms.providerMultiplexer.getProviderMultiplexerFunctionTable.function()

// Call the 'main' function while also handling any errors by logging them to the console
main().catch((errorMessage) => console.log(errorMessage))

// Define the main function to be asynchronous to use 'await' based Promise calls
async function main () {
	await database.createItem.function('store-id', 'item-id', { message: 'hello world' })
	const item = await database.getItem.function('store-id', 'item-id')
	console.log('Retrieved item from database: ', item)
}
```

### Application Programmable Interface (API Reference)

<pre><details open><summary>📁JavaScript Library API Reference (Click to open)</summary>

<!-- Provider Multiplexer -->
<pre><details><summary>📁Provider Multiplexer API Reference</summary>
<!-- Algorithms -->
<pre><details><summary>📁Algorithms</summary>
Algorithms Information
</details></pre>
<!-- Constants -->
<pre><details><summary>📁Constants</summary>
Constants Information
</details></pre>
<!-- Data Structures -->
<pre><details><summary>📁Data Structures</summary>
Data Structures Information
</details></pre>
<!-- Variables -->
<pre><details><summary>📁Variables</summary>
Variables
</details></pre>
</details></pre>

<!-- End of JavaScript Library Reference-->
</details></pre>

### Application Protocol Providers and 3rd Party Services


✅ Completed integration support. Providers with this marker are already integrated in this project.

🚧 Pending integration support. Providers with this marker are planned to be integrated in future releases of this project.


### Asymmetric Cryptography Provider
✅ 1 of 1 Providers Completed for Planned Integration

| Provider Name | ✅ **elliptic** |
| - | - |
| Provider Author | Fedor Indutny et al. |
| Provider Resource List | [https://github.com/indutny/elliptic](https://github.com/indutny/elliptic) | 

### Computer Resource Service Provider
✅ 0 of 7 Providers Completed for Planned Integration

| Provider Name | 🚧 **Glitch** |  🚧 **Heroku** |  🚧 **Microsoft Azure** |  🚧**Digital Ocean** |  🚧 **Google Cloud** | 🚧 **Amazon Web Services** | 🚧 **Browserstack**
| - | - | - | - | - | - | - | - |
| Provider Author | [https://glitch.com](https://glitch.com) | [https://www.heroku.com](https://www.heroku.com) | [https://azure.microsoft.com/en-us](https://azure.microsoft.com/en-us) | [https://developers.digitalocean.com](https://developers.digitalocean.com) | [https://cloud.google.com/apis](https://cloud.google.com/apis) | [https://aws.amazon.com](https://aws.amazon.com) | [https://www.browserstack.com](https://www.browserstack.com)
| Provider Resource List | [https://github.com/GlitchDotCom](https://github.com/GlitchDotCom), (Integrated via [Open Pilot](https://gitlab.com/practicaloptimism/pilot)) | [https://devcenter.heroku.com/categories/platform-api](https://devcenter.heroku.com/categories/platform-api) | [https://github.com/Azure/azure-sdk-for-js](https://github.com/Azure/azure-sdk-for-js) | [https://github.com/matt-major/do-wrapper](https://github.com/matt-major/do-wrapper) | [https://github.com/googleapis/nodejs-compute](https://github.com/googleapis/nodejs-compute) | [https://www.npmjs.com/package/aws-sdk](https://www.npmjs.com/package/aws-sdk) | [https://github.com/scottgonzalez/node-browserstack](https://github.com/scottgonzalez/node-browserstack) |

### Cryptographic Hash Function Provider
✅ 2 of 2 Providers Completed for Planned Integration

| Provider Name | ✅ **object-hash** | ✅ **crypto-js** |
| - | - | - |
| Provider Author | Anna Henningsen et al. | Evan Vosberg et al. |
| Provider Resource List | [https://github.com/puleos/object-hash](https://github.com/puleos/object-hash) | [https://github.com/brix/crypto-js](https://github.com/brix/crypto-js) |

### Currency Service Provider
✅ 0 of 2 Providers Completed for Planned Integration

| Provider Name | 🚧 **Stripe** | 🚧 **PayPal** |
| - | - | - |
| Provider Author | [https://stripe.com](https://stripe.com) | Elon Musk, Peter Thiel, et al. [http://paypal.com](http://paypal.com) |
| Provider Resource List | [https://github.com/stripe/stripe-node](https://github.com/stripe/stripe-node) | [https://github.com/paypal/Payouts-NodeJS-SDK](https://github.com/paypal/Payouts-NodeJS-SDK), [https://github.com/paypal/Checkout-NodeJS-SDK](https://github.com/paypal/Checkout-NodeJS-SDK) |

### Database Provider
✅ 1 of 9 Providers Completed for Planned Integration

| Provider Name | ✅ **localForage** |  🚧 **node-persist** | 🚧 **orbit-db** | 🚧 **WebDB** | 🚧 **Firebase Realtime Database** & **Firebase Cloud Firestore** | 🚧 **CouchDB** | 🚧 **MongoDB** | 🚧 **Google Cloud DataStore** | 🚧 **Google Cloud Storage** |
| - | - | - | - | - | - | - | - | - | - |
| Provider Author | Matthew Riley MacPherson et al. | Ben Monro, Aziz Khoury et al. | Haad (@haadcode) | Paul Frazee, Mathius Buus, Tara Vancil et al. | [https://firebase.google.com](https://firebase.google.com) | [https://couchdb.apache.org](https://couchdb.apache.org) | [https://www.mongodb.com](https://www.mongodb.com) | [https://cloud.google.com](https://cloud.google.com) | [https://cloud.google.com](https://cloud.google.com) |
| Provider Resource List | [https://github.com/localForage/localForage](https://github.com/localForage/localForage) | [https://github.com/simonlast/node-persist](https://github.com/simonlast/node-persist) | [https://github.com/orbitdb/orbit-db](https://github.com/orbitdb/orbit-db) | [https://github.com/beakerbrowser/webdb](https://github.com/beakerbrowser/webdb) | [https://github.com/firebase/firebase-js-sdk](https://github.com/firebase/firebase-js-sdk) | [https://github.com/apache/couchdb-nano](https://github.com/apache/couchdb-nano) | [https://github.com/mongodb/node-mongodb-native](https://github.com/mongodb/node-mongodb-native) | [https://www.npmjs.com/package/@google-cloud/datastore](https://www.npmjs.com/package/%40google-cloud/datastore)| [https://googleapis.dev/nodejs/storage/latest](https://googleapis.dev/nodejs/storage/latest) |

### Database / File System Provider
✅ 0 of 11 Providers Completed for Planned Integration

| Provider Name | 🚧 **BrowserFS** | 🚧 **IPFS** |  🚧 **DatArchive** |  🚧 **WebTorrent** | 🚧 **Firebase Cloud Storage** | 🚧 **Google Drive** | 🚧 **Perkeep** | 🚧 **Gitlab** | 🚧 **Neocities** | 🚧 **Service Worker** | 🚧 **Vercel** |
| - | - | - | - | - | - | - | - | - | - | - | - |
| Provider Author | John Vilk et al. | Juan Benet et al. | Paul Frazee, Mathius Buus, Tara Vancil et al. | Feross Aboukhadijeh et al. | [https://firebase.google.com](https://firebase.google.com) | [https://developers.google.com/drive](https://developers.google.com/drive) | Brad Fitzgerald et al. | Sid Sijbrandij et al. | Kyle Drake et al. | - | [https://vercel.com](https://vercel.com) |
| Provider Resource List | [https://github.com/jvilk/BrowserFS](https://github.com/jvilk/BrowserFS) | [https://github.com/ipfs/js-ipfs](https://github.com/ipfs/js-ipfs) | [https://beakerbrowser.com/docs/apis/dat.html](https://beakerbrowser.com/docs/apis/dat.html) | [https://github.com/webtorrent/webtorrent](https://github.com/webtorrent/webtorrent) | [https://github.com/firebase/firebase-js-sdk](https://github.com/firebase/firebase-js-sdk) | [https://www.npmjs.com/package/googleapis](https://www.npmjs.com/package/googleapis) | [https://github.com/perkeep/perkeep](https://github.com/perkeep/perkeep) | [https://github.com/jdalrymple/gitbeaker](https://github.com/jdalrymple/gitbeaker) | [https://github.com/neocities/neocities-node](https://github.com/neocities/neocities-node) | [https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers) | [https://vercel.com/docs/api](https://vercel.com/docs/api) |


### Distributed Ledger Technology Provider
✅ 0 of 1 Providers Completed for Planned Integration

| Provider Name | 🚧 **Hashgraph** |
| - | - |
| Provider Author | Jon Ide et al. |
| Provider Resource List | [https://gitlab.com/practicaloptimism/hashgraph](https://gitlab.com/practicaloptimism/hashgraph) |

### Message Streaming Service Provider
✅ 0 of 1 Providers Completed for Planned Integration

| Provider Name | 🚧 **Slack** |
| - | - |
| Provider Author | [https://slack.com](https://slack.com) |
| Provider Resource List | [https://slack.dev/node-slack-sdk/web-api](https://slack.dev/node-slack-sdk/web-api) |


### Network Protocol Provider
✅ 0 of 3 Providers Completed for Planned Integration

| Provider Name |  🚧 **WebRTC** |  🚧 **WebSocket** | 🚧 **HTTP**
| - | - | - | - |
| Provider Author | Justin Uberti et al. | [https://www.ietf.org](https://www.ietf.org) | Tim Berners-Lee et al. |
| Provider Resource List | [https://github.com/feross/simple-peer](https://github.com/feross/simple-peer) | [https://github.com/websockets/ws](https://github.com/websockets/ws) | [https://github.com/axios/axios](https://github.com/axios/axios) |

### Text Search Provider
✅ 0 of 3 Providers Completed for Planned Integration

| Provider Name | 🚧 **Solr** | 🚧 **Lunr** |  🚧 **Flexsearch**
| - | - | - | - |
| Provider Author | Remy Loubradou et al. | Oliver Nightingale et al. | Thomas Wilkerling et al. |
| Provider Resource List | [https://github.com/lbdremy/solr-node-client](https://github.com/lbdremy/solr-node-client) | [https://github.com/olivernn/lunr.js](https://github.com/olivernn/lunr.js) | [https://github.com/nextapps-de/flexsearch](https://github.com/nextapps-de/flexsearch) |

### Operating Environemts for JavaScript Runtime Environments

| JavaScript Runtime Environment | Node.js | Google Chrome | Mozilla Firefox | Opera Browser | Apple Safari | Bluelink Labs Beaker Browser | Microsoft Edge | Brave Browser | Deno |
| -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
| Supported Versions of the Runtime | The latest version | The latest version | The latest version | The latest version | The latest version | The latest version | The latest version | The latest version | No supported versions | 

### To Resolve Problems and Submit Feature Requests

To report issues or submit feature requests with this project, please visit [Uniplexer's Gitlab Issue Tracker](https://gitlab.com/practicaloptimism/uniplexer/issues)

### About This Project
**Uniplexer** is a multiplexer for third party services and application protocols.

**Multiplexers** are instruments or tools that allow you to **toggle or select from a list of multiple services and protocols** including heavifying from one feature or another from service to service at the cost of providing a credential or set of credentials for those services you're using.

**3rd Party Services** such as _database_, _computer_, and _distributed ledger technology_ services are numerous in availability in the community and yet having a multiplexer to provide **one single interface to all those services** helps to reduce the complexity of accessing the services you need when you need them.

**Application Protocols** are diverse and require various needs depending on the protocol implementation. **A common protocol interface for any set of specific protocols** is tremendously useful to make transitioning to new protocols easy and fast for experiencing less developer boredom if and when you need to make the transition.

### Benefits
* 🌈 **Programmatically Select Program Architecture**: By using `provider credential ids` in your function calls, you can toggle between different providers that you would have otherwise had to choose between by defining a default set of providers to handle various operations of your software requirements.

### Features
* 🔧 **Create Your Own Provider Types**: Optionally organize your own providers with a provider type that you define and use this project's multiplexer to select between your own providers at runtime without waiting for us to add support for that provider or provider type.
* 🚗 **Select From a Set of Provider Types**: Database, Computer Resource Service, Cryptographic Hash Function and many other predefined provider types to select from.
* 🎥 **Create Your Own Providers**: Create your own providers using the provider data structure we provide to organize the providers you depend on for your application.
* 📺 **Select From a Set of Providers**: Use predefined providers that are organized to have the same function names for any providers with the same provider type


### Limitations
* 🤓 **Work-in-progress**: This project is a work-in-progress. The project architecture and documentation are being updated regularly. Please learn more about the development life cycle by visiting our **live programming development sessions on youtube**: [https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g](https://www.youtube.com/channel/UCIv-rMXljsbxoTUg1MXBi3g)

### Related Work
There are many projects relating to the application usecases that Uniplexer strives to provide. A "**currency service multiplexer**", a "**database multiplexer**", a "**network protocol multiplexer**", and many more usecase provider multiplexers are the primary goal for Uniplexer. Here is a non-exhaustive list of other projects in the world of multiplexer libraries that are being used and also relate to Uniplexer's usecase areas-of-interest (well renowned projects are prioritized in our listing order strategy):

| Currency Service Multiplexer | Database Multiplexer | Network Protocol Multiplexer |
| -- | -- | -- | 
| [1] **Coinpayments**, Mark Paroski et al. [https://github.com/OrahKokos/coinpayments](https://github.com/OrahKokos/coinpayments) | [1] **Localforage**, Mathew Riley MacPherson et al. [https://github.com/localForage/localForage](https://github.com/localForage/localForage) | [1] **libp2p**, Juan Benet et. al [https://github.com/libp2p](https://github.com/libp2p) | 
| [2] **Uniplexer**, Jon Ide et al. [https://gitlab.com/practicaloptimism/uniplexer](https://gitlab.com/practicaloptimism/uniplexer) | [2] **BrowserFS**, John Vilk et al. [https://github.com/jvilk/BrowserFS](https://github.com/jvilk/BrowserFS) | [2] **Uniplexer**, Jon Ide et al. [https://gitlab.com/practicaloptimism/uniplexer](https://gitlab.com/practicaloptimism/uniplexer) |
| -- | [3] **valkeyrie**, Alexandre Belsic et al. [https://github.com/abronan/valkeyrie](https://github.com/abronan/valkeyrie) | -- |
| -- | [4] **Uniplexer**, Jon Ide et al. [https://gitlab.com/practicaloptimism/uniplexer](https://gitlab.com/practicaloptimism/uniplexer) | -- |

### Acknowledgements


### License
MIT
