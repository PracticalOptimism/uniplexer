# Development Journal
This project is a **work-in-progress**. 

##### Progress Report Legendor Map
| Number of Stars | Progress Report Description |
|--|--|
| 👶| still need to do research on this topic |
| ⭐ | have done research and just need to write programs |
| ⭐⭐ | wrote a few algorithms or tests |
| ⭐⭐⭐ | finished the algorithms and tests |
| 👽👽👽| ready for production environment usecases, ie. confident usecases have been tested in demos |


##### Progress Report as of April 17, A.D. 2020
| Number of Stars | Project Topic Title | Detailed Report Description |
|----|----|----|
|⭐⭐| Asymmetric Cryptography | |
|⭐| Chat Service |  |
| 👶 | Computer Service |  |
|⭐⭐| Database |  |
| 👶 | Distributed Ledger Technology |  |
| 👶 | File System |  |
|⭐⭐| Hash Function |  |
| 👶 | Network Protocol |  |
|⭐⭐| Provider  |  |
|⭐| Text Search  |  |
